"""
#TODO(eko): rewrite to put numpy arrays and dataset into HDF5 file
#TODO(eko): rewrite to put numpy arrays and dataset into HDF5 file
#TODO(eko): rewrite to put numpy arrays and dataset into HDF5 file
#TODO(eko): rewrite to put numpy arrays and dataset into HDF5 file
#TODO(eko): rewrite to put numpy arrays and dataset into HDF5 file
#TODO(eko): rewrite to put numpy arrays and dataset into HDF5 file
"""


import vol_img_helpers
import glob
import os, math
import cv2  # conda install -c https://conda.anaconda.org/menpo opencv3
import scipy.misc
import pydicom  # pip install pydicom
import numpy as np
import pandas as pd
import json
from collections import defaultdict
from joblib import Parallel, delayed
from itertools import compress
from tqdm import tqdm
import threading

import logging
logging.basicConfig(
  level=logging.DEBUG,
  format='%(asctime)s - %(thread)d - %(name)s - %(levelname)s - %(message)s',
  filename='/home/eko/projects/icahnc_cth/cth_cnn/logs/3dcnn_dataset.log',)
logger = logging.getLogger(__name__)


def func(x):
    time.sleep(random.randint(1, 10))
    return x

def text_progessbar(seq, total=None):
    step = 1
    tick = time.time()
    while True:
        time_diff = time.time()-tick
        avg_speed = time_diff/step
        total_str = 'of %n' % total if total else ''
        print('step', step, '%.2f' % time_diff, 
              'avg: %.2f iter/sec' % avg_speed, total_str)
        step += 1
        yield next(seq)

all_bar_funcs = {
    'tqdm': lambda args: lambda x: tqdm(x, **args),
    'txt': lambda args: lambda x: text_progessbar(x, **args),
    'False': lambda args: iter,
    'None': lambda args: iter,
}

def ParallelExecutor(use_bar='tqdm', **joblib_args):
    def aprun(bar=use_bar, **tq_args):
        def tmp(op_iter):
            if str(bar) in all_bar_funcs.keys():
                bar_func = all_bar_funcs[str(bar)](tq_args)
            else:
                raise ValueError("Value %s not supported as bar type"%bar)
            return Parallel(**joblib_args)(bar_func(op_iter))
        return tmp
    return aprun


def screen_slices_for_orientation(slices, axis):
  """
  Screens a list of dicom slices for orientation. !! Currently only does axial
  """
  image_orientation_screen = []
  series_description_screen = []
  series_desc_list = ["standard", "axial", "h31s", "h41s", "routine"]
  for i in range(len(slices)):
    try:
      #print(slices[i].ImageOrientationPatient)
      if (round(slices[i].ImageOrientationPatient[0]) == 1) & (round(slices[i].ImageOrientationPatient[4]) == 1):
        image_orientation_screen.append(True)
      else:
        image_orientation_screen.append(False)
    except AttributeError as e:
      image_orientation_screen.append(False)
    try:
      #print(slices[i].SeriesDescription)
      if  any(series_desc in slices[i].SeriesDescription.lower() for series_desc in series_desc_list):
        series_description_screen.append(True)
      else:
        series_description_screen.append(False)
    except AttributeError as e:
      series_description_screen.append(False)
  #get the joint list of scans that meet inclusion    
  inclusion_mask = [a and b for a, b in zip(image_orientation_screen, series_description_screen)]
  #mask the slices list
  cleaned_slices=list(compress(slices, inclusion_mask))
  return cleaned_slices


def screen_slices_for_series_description(slices):
  """
  """
  series_description_blacklist=["topo", "patient protocol", "scout"]
  cleaned_slices=[]
  for current_slice in slices:
    if all(x not in current_slice.SeriesDescription.lower() for x in series_description_blacklist):
        cleaned_slices.append(current_slice)
  return cleaned_slices   


def standardize_slice_thickness(slices):
  """
  slice_thickness_calc = calculated from the average difference in slice position
  slice_thickness_recorded = recorded on each slice in metadata
  """
  #Manually calculate from average through slices
  slice_thickness_calc=[]
  slice_thickness_recorded=[]
  for i in range(len(slices)-1):
    #GET CALCULATED FOR THIS SLICE
    try:
      slice_thickness = np.abs(slices[i].ImagePositionPatient[2] - slices[i+1].ImagePositionPatient[2])
    except:
      slice_thickness = np.abs(slices[i].SliceLocation - slices[i+1].SliceLocation)
    slice_thickness_calc.append(slice_thickness)
    #GET RECORDED THICKNESS
    try:
      slice_thickness_recorded.append(slices[i].SliceThickness)
    except:
      #we append a 5 rather than pass b/c we don't want to have an empty list and handle npy exception later
      slice_thickness_recorded.append(5)

  #Get averages
  slice_thickness_calc_avg = np.average(slice_thickness_calc)
  slice_thickness_recorded_avg = np.average(slice_thickness_recorded)
  logger.debug("slice_thickness_calc_avg:{0}".format(slice_thickness_calc_avg))
  logger.debug("slice_thickness_recorded_avg:{0}".format(slice_thickness_recorded_avg))
  #we'll use the calculated unless it is aberrant, in which case we'll default to the recorded, which if it wasn't recorded will be =5mm
  if slice_thickness_calc_avg < 10.0:
    slice_thickness = slice_thickness_calc_avg
  else:
    slice_thickness = slice_thickness_recorded_avg
  #set slicethickness for all slice to standardized one
  for s in slices:
    s.SliceThickness = slice_thickness

  return slices


def load_patient(src_dir,axis,screen_flag=True):
    #check that file is dicom file
    #slices = [pydicom.read_file(src_dir + '/' + s) for s in os.listdir(src_dir) ]
    slices = []
    for s in os.listdir(src_dir):
        if s.endswith('.dcm'):
            current_slice=pydicom.read_file(src_dir + '/' + s)
            slices.append(current_slice) 

    if screen_flag:
      logger.debug("num slices before screen:{0}".format(len(slices)))
      slices = screen_slices_for_series_description(slices)
      if len(slices) == 0:
        raise IndexError("Slice list empty, no viable images found.")
        return
      slices = screen_slices_for_orientation(slices,axis)
      if len(slices) == 0:
        raise IndexError("Slice list empty, no viable images found.")
        return

    logger.debug("num slices after orientation_screen:{0}".format(len(slices)))
    slices.sort(key=lambda x: int(x.InstanceNumber)) #sort into order

    slices = standardize_slice_thickness(slices)

    #TODO(eko): no need to screen on dim b/c going to cut out cubes later
    #if not standard dim, toss
    # cleaned_slices = []
    # for s in slices:
    #     if s.pixel_array.shape[0] == 512:
    #         if s.pixel_array.shape[1] == 512:
    #             cleaned_slices.append(s)
    # slices=cleaned_slices
    # print("num slices after xy_screen:", len(slices))
    return slices


def get_pixels_hu(slices):
    image = np.stack([s.pixel_array for s in slices])
    image = image.astype(np.int16)
    image[image <= -2000] = 0
    for slice_number in range(len(slices)):
        try:
            intercept = slices[slice_number].RescaleIntercept
            stored_intercept = intercept
        except AttributeError as e:
            #If we through an attributeerror, we'll set intercept to be the last good one
            intercept = stored_intercept
        except AttributeError as e:
            intercept = "-1024"
        try:
            slope = slices[slice_number].RescaleSlope
            stored_slope = slope
        except AttributeError as e:
            #If we through an attributeerror, we'll set intercept to be the last good one
            slope = stored_slope
        if slope != 1:
            image[slice_number] = slope * image[slice_number].astype(np.float64)
            image[slice_number] = image[slice_number].astype(np.int16)
        image[slice_number] += np.int16(intercept)
    return np.array(image, dtype=np.int16)


def resample(image, pixel_spacing, new_spacing=[1, 1, 1], verbose=False):
    spacing = np.array(list(pixel_spacing))
    resize_factor = spacing / new_spacing
    new_real_shape = image.shape * resize_factor
    new_shape = np.round(new_real_shape)
    real_resize_factor = new_shape / image.shape
    new_spacing = spacing / real_resize_factor
    image = scipy.ndimage.interpolation.zoom(image, real_resize_factor)
    if verbose:
      logger.debug("New image_shape:{0}".format(image.shape))
    return image, new_spacing


def cv_flip(img,cols,rows,degree):
    M = cv2.getRotationMatrix2D((cols / 2, rows /2), degree, 1.0)
    dst = cv2.warpAffine(img, M, (cols, rows))
    return dst


def extract_dicom_images_patient(src_dir=None, input_dir=None, output_dir=None, 
  target_voxel_spacing=[1.0,1.0,1.0], normalize=True, cut_cubes=True, target_dim=140,
   save_type="array", verbose=True, screen_flag=True):
  """
  This method makes an isotropic array out of a given standard DICOMs shot
    Args:
    Returns:
      bt_dict: a dictionary containing entries for the big table
  """
  dir_path = os.path.join(input_dir,src_dir)
  sopinstanceuid = src_dir
  logger.debug("dir_path:{0}".format(dir_path))
  #TODO(eko): currently defaulting this to axial
  #NB: will try to load DCMs, if error, will return the NaN dict
  try:
    slices = load_patient(dir_path,axis="axial",screen_flag=screen_flag)
    if len(slices) == 0:
      raise IndexError("Slice list empty, no viable images found.")
  except IndexError as e:
    logger.warning("dir_path:{0}| NO VIABLE IMAGES".format(dir_path))
    bt_dict = {
      "img_path":np.nan,
      "sopinstanceuid":np.nan,
      "patient_age":np.nan,
      "patient_sex":np.nan,
      "patient_id":np.nan,
      "patient_accession":np.nan,
      "original_dims":np.nan,
      "post_rescale_dims":np.nan,
      "post_cube_dims":np.nan,
      "final_shape":np.nan}
    return bt_dict

  try:
    image = get_pixels_hu(slices)
  except ValueError as e:
    logger.warning("dir_path:{0}| IMAGES INCONSISTENT SHAPES".format(dir_path))
    bt_dict = {
      "img_path":np.nan,
      "sopinstanceuid":np.nan,
      "patient_age":np.nan,
      "patient_sex":np.nan,
      "patient_id":np.nan,
      "patient_accession":np.nan,
      "original_dims":np.nan,
      "post_rescale_dims":np.nan,
      "post_cube_dims":np.nan,
      "final_shape":np.nan}
    return bt_dict

  logger.debug("NumSlices:{0}, SliceThickness:{1}, PixelSpacing:{2}".format(len(slices),slices[0].SliceThickness,slices[0].PixelSpacing))
  logger.debug("Orientation:{0}".format(slices[0].ImageOrientationPatient))
  #assert slices[0].ImageOrientationPatient == [1.000000, 0.000000, 0.000000, 0.000000, 1.000000, 0.000000]
  cos_value = (slices[0].ImageOrientationPatient[0])
  cos_degree = round(math.degrees(math.acos(cos_value)),2)
  
  #get patient accession# (patient_id) for bt_dict
  try:
    patient_age = str(slices[0].PatientAge)
  except AttributeError as e:
    patient_age = "00"
  try:
    patient_sex = str(slices[0].PatientSex)
  except AttributeError as e:
    patient_sex = "NA"
  try:
    patient_id = str(slices[0].PatientID)
  except AttributeError as e:
    patient_id = "0000000"
  try:
    patient_accession = str(slices[0].AccessionNumber)
  except AttributeError as e:
    patient_accession = "0000000"  

  original_dims = image.shape #get original shape

  logger.debug("Image shape:{0}".format(original_dims))

  try:
    invert_order = slices[1].ImagePositionPatient[2] > slices[0].ImagePositionPatient[2]
  except IndexError as e:
    logger.warning("dir_path:{0}| IMAGES INCONSISTENT DIMS ON INVERT_ORDER".format(dir_path))
    bt_dict = {
      "img_path":np.nan,
      "sopinstanceuid":np.nan,
      "patient_age":np.nan,
      "patient_sex":np.nan,
      "patient_id":np.nan,
      "patient_accession":np.nan,
      "original_dims":np.nan,
      "post_rescale_dims":np.nan,
      "post_cube_dims":np.nan,
      "final_shape":np.nan}
    return bt_dict


  logger.debug("Invert order:{0} - {1}, {2}".format(invert_order,slices[1].ImagePositionPatient[2],slices[0].ImagePositionPatient[2]))

  pixel_spacing = slices[0].PixelSpacing
  pixel_spacing.insert(0,slices[0].SliceThickness)
  #rescale to mm  
  image, new_spacing = resample(image, pixel_spacing, new_spacing=target_voxel_spacing, verbose=verbose)

  post_rescale_dims = image.shape #get post rescale dims

  if not invert_order:
    image = np.flipud(image)

  patient_dir = output_dir+"/vols"
  if not os.path.exists(patient_dir):
    os.mkdir(patient_dir)
  # if there exists slope,rotation image with corresponding degree
  if cos_degree>0.0:
    image = cv_flip(image,image.shape[1],image.shape[0],cos_degree)
  # TODO(eko): going to normalize here... note that I'm losing the Hounsefields tho
  if cut_cubes:
    logger.debug("Image dims prior to cubing:{0}".format(image.shape))
    image = vol_img_helpers.get_cubed_vol(image, target_dim=target_dim)
    post_cube_dims = image.shape
  if normalize:
    #org_img = vol_img_helpers.normalize_hu(image)
    image = vol_img_helpers.get_normalized_img_unit8(image)
  if save_type == "png":
    for i in range(image.shape[0]):
      img_path = patient_dir+"/{0}_img_{1}.png".format(sopinstanceuid, str(i).rjust(4, '0'))
      if verbose:
        logger.debug("saving:{0} to: {1}".format(patient_dir, img_path))
      cv2.imwrite(img_path, image)
  if save_type == "array":
    img_path = patient_dir+"/{0}.npy".format(sopinstanceuid)
    if verbose:
      logger.debug("saving:{0} of dims {1} to:{2}".format(patient_dir,image.shape,img_path))
    np.save(img_path,image)

  bt_dict = {
    "img_path":img_path,
    "sopinstanceuid":sopinstanceuid,
    "patient_age":patient_age,
    "patient_sex":patient_sex,
    "patient_id":patient_id,
    "patient_accession":patient_accession,
    "original_dims":original_dims,
    "post_rescale_dims":post_rescale_dims,
    "post_cube_dims":post_cube_dims,
    "final_shape":image.shape}
  
  threadsafe_json_writer(
    json_file=os.path.join(output_dir,"metadata/bigtable_{0}.json".format(sopinstanceuid)),
    json_entry=bt_dict)


def threadsafe_json_writer(json_file=None, json_entry=None):
  with open(json_file, 'w') as f:
    json.dump(json_entry, f)


def extract_dicom_images(clean_targetdir_first=False, only_patient_id=None, input_dir=None, output_dir=None,
                         target_voxel_spacing=[1.0,1.0,1.0], normalize=False, cut_cubes=True, target_dim=140,
                         save_type="array", verbose=True, screen_flag=True, parallelize=False, n_jobs=6, n_shards=4,
                         output_type="pandas"):
  """
  This is a parallelized dicom extraction/standardization method. It will take a target directory of dicom images,
  input_dir, and return a directory of reformatted, isotropic, uniformly cubed numpy arrays, output_dir.arrays
  Args:
    clean_targetdir_first: bool, specifies whether to delete contents of target directory prior to beginning
    only_patient_id: str, a single    
  """

  logger.debug("Extracting images")
  #Will automatically clean up anything in the target dir (be careful with this)
  if clean_targetdir_first and only_patient_id is not None:
    logger.debug("Cleaning target dir")
    for file_path in glob.glob(output_dir + "*.*"):
      os.remove(file_path)

  if only_patient_id is None:
    total_dirs = os.listdir(input_dir)
    sharded_dirs = [total_dirs[i:i+len(total_dirs)//n_shards] for i in range(0,len(total_dirs),len(total_dirs)//n_shards)]
    
    last_run = 0
    total_processed = 0
    print("PROCESSING {0} SHARDS OF LENGTH {1}".format(len(sharded_dirs), len(sharded_dirs[0])))
    for dirs in sharded_dirs:

      #HACK
      dirs=dirs[5400:]

      if parallelize:
        #TODO(eko): sort out multiprocessing
        aprun = ParallelExecutor(n_jobs=n_jobs)
        aprun(total=len(dirs))(delayed(extract_dicom_images_patient)(
          src_dir=dir_entry,
          input_dir=input_dir,
          output_dir=output_dir,
          target_voxel_spacing=target_voxel_spacing,
          normalize=normalize,
          cut_cubes=cut_cubes,
          target_dim=target_dim,
          save_type=save_type,
          verbose=verbose,
          screen_flag=screen_flag) for dir_entry in dirs)

        if output_type == "pandas":
          metadata = []
          for json_file in [os.path.join(output_dir,"metadata",i) for i in os.listdir(os.path.join(output_dir,"metadata"))]:
            with open(json_file) as jf:
              metadata.append(json.load(jf))
   
          df_bigtable=pd.DataFrame(metadata)
          logger.debug("SAVING BigTable.csv")
          df_bigtable.to_csv(os.path.join(output_dir,"BigTable.csv"))

        if output_type == "json":        
          with open(os.path.join(output_dir,"BigTable_{0}-{1}.json".format(last_run,total_processed)), 'w') as fp:
            json.dump(bt_dict, fp)

      else:
        bigtable = []
        for dir_entry in dirs:
          bt_dict = extract_dicom_images_patient(
            src_dir=dir_entry,
            input_dir=input_dir,
            output_dir=output_dir,
            target_voxel_spacing=target_voxel_spacing,
            normalize=normalize,
            cut_cubes=cut_cubes,
            target_dim=target_dim,
            save_type=save_type,
            verbose=verbose,
            screen_flag=screen_flag)
          bigtable.append(bt_dict)
        #kind of ghetto, but just keeping track of how many we've run through
        total_processed+=len(dirs)

        if output_type == "pandas":
          df_bigtable=pd.DataFrame(bt_dict)
          logger.debug("SAVING BigTable_{0}-{1}.csv".format(last_run,total_processed))
          df_bigtable.to_csv(os.path.join(output_dir,"BigTable_{0}-{1}.csv".format(last_run,total_processed)))

        if output_type == "json":        
          with open(os.path.join(output_dir,"BigTable_{0}-{1}.json".format(last_run,total_processed)), 'w') as fp:
            json.dump(bt_dict, fp)

        last_run+=total_processed


if __name__ == "__main__":

  #Import for time in case we want to time our stuff

  import time
  start_time = time.time()

  extract_dicom_images(
    clean_targetdir_first=False,
    only_patient_id=None,
    input_dir="/home/eko/data/ICAHNC1/imaging",
    output_dir="/home/eko/projects/icahnc_cth/cth_cnn/data/train_normed",
    target_voxel_spacing=[1.0,1.0,1.0],
    normalize=False,
    cut_cubes=True,
    target_dim=128,
    save_type="array",
    verbose=True,
    screen_flag=True, #screens each slice for proper orientation
    parallelize=True,
    n_jobs=12,
    n_shards=1,
    output_type="pandas")

  print("--- {0} seconds ---".format(time.time()-start_time))