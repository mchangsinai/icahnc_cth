"""
LICENSING:
Copyright 2016, Mount Sinai Health System. All rights reserved.

This software is part of the ICAHNC group source code in the Mount Sinai Health System (MSHS).
Redistribution and use in source and binary forms, with or without modification, are not permitted 
without the explicit and written consent of the ICAHNC Principal Investigators (EKO, JT) or designated
representatives.

ICAHNC DEV TEAM:
Eric Karl Oermann, M.D.
Marcus Badgeley, M.Eng.

LIBRARY SUMMARY:
***
"""

import sys
import keras
from keras.models import Sequential, Model
from keras.layers import Dense, Dropout, Activation, Flatten, Input, Merge, Cropping3D, Embedding, Concatenate
from keras.layers import Conv3D, MaxPooling3D, BatchNormalization, AveragePooling3D, ZeroPadding2D, ZeroPadding3D, Convolution2D, MaxPooling2D
from keras.optimizers import SGD, Adagrad, Adam

#TODO(eko): make the NLP libraries a more general directory we can all call from/work out of
sys.path.insert(0,'/home/eko/lib')
from keras_helpers import multi_gpu
#import volumetric_resnet_elu
import volumetric_resnet

import logging
logger = logging.getLogger()
logger.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
fh = logging.FileHandler('/home/eko/projects/icahnc_cth/cth_cnn/logs/cth_3dcnn.log')
fh.setLevel(logging.DEBUG)
fh.setFormatter(formatter)
logger.addHandler(fh)
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
ch.setFormatter(formatter)
logger.addHandler(ch)

#TODO(ekoermann): clear up input shapes to take the input from the current json/datasource
#TODO(ekoermann): REDO 3DVGG with residuals like resnet... a lot of dead weight

def Conv3DBatchNormRelu(n_filter, w_filter, h_filter, activation, name, inputs):
    #EX: cnn = Conv3DBatchNormRelu(32, 5, 5, 5, name="conv1_1", input=cnn)
    return BatchNormalization()(Activation(activation=activation)(Convolution3D(n_filter, d_filter, w_filter, h_filter, border_mode='same', name=name)(inputs)))


def model_builder(model_name="", num_classes=10, input_shape=(32,32,32,1), multilabel=False,
    gpu_data_parallelize=False, gpu_count=1, downsample=False, source_type="json"):


  if model_name == "voxnet":
    if source_type=="json":
      vol_input = Input(shape=input_shape)
      x = AveragePooling3D(pool_size=(1, 4, 4), strides=None, padding='valid', name="avg_1")(vol_input)
      x = Activation('relu')(BatchNormalization()(Conv3D(filters=32, kernel_size=(5, 5, 5), padding='same', name="conv1_1")(x)))
      x = Dropout(0.05)(BatchNormalization()(MaxPooling3D(pool_size=(4,4,4))(x)))
      x = Flatten()(x)
      x = Dropout(0.05)(Dense(128)(x))
      if not multilabel:
        classes = Dense(num_classes, activation="softmax", name='classes')(x)
      else:
        classes = Dense(num_classes, activation="sigmoid", name='classes')(x)
      model = Model(inputs=vol_input, outputs=classes)

    if source_type == "multimodal":
      vol_input = Input(shape=input_shape)
      x = AveragePooling3D(pool_size=(1, 4, 4), strides=None, padding='valid', name="avg_1")(vol_input)
      x = Activation('relu')(BatchNormalization()(Conv3D(filters=32, kernel_size=(5, 5, 5), padding='same', name="conv1_1")(x)))
      x = Dropout(0.05)(BatchNormalization()(MaxPooling3D(pool_size=(4,4,4))(x)))
      x = Flatten()(x)
      x_img = Dropout(0.05)(Dense(128)(x))

      #introduce and embed auxillary inputs
      age_input = Input(shape=(1,), name="age_input")
      gen_input = Input(shape=(1,), name="gen_input")
      x_age = Embedding(output_dim=10, input_dim=100, input_length=1)(age_input)
      x_age = Flatten()(x_age)
      x_gen = Embedding(output_dim=10, input_dim=2, input_length=1)(gen_input)
      x_gen = Flatten()(x_gen)
      #concatenate aux_inputs to main image input
      x = keras.layers.concatenate([x_img, x_age, x_gen])

      if not multilabel:
        classes = Dense(num_classes, activation="softmax", name='classes')(x)
      else:
        classes = Dense(num_classes, activation="sigmoid", name='classes')(x)
      model = Model(inputs=[vol_input, gen_input, age_input], outputs=classes)
    
  if model_name == "3dresnet_modelnet":
    model = volumetric_resnet.volumetric_resnet50_modelnet(num_classes, input_shape)

  if model_name == "3dresnet_elu":
    model = volumetric_resnet_elu.volumetric_resnet50(num_classes, input_shape)
    
  if model_name == "volumetric_resnet25":
    model = volumetric_resnet.volumetric_resnet25(num_classes, input_shape, downsample, multilabel)

  if model_name == "volresnet":
    if source_type != "multimodal":
      model = volumetric_resnet.volresnet(num_classes, input_shape, downsample, multilabel)
    elif source_type == "multimodal":
      model = volumetric_resnet.mm_volumetric_resnet50(num_classes, input_shape, downsample, multilabel)
    else:
      Exception("Wrong source type")

  if model_name == "3dresnet101":
    model = volumetric_resnet_elu.volumetric_resnet101(num_classes, input_shape)
  
  #if model_name == "3DResNet":
    #MODEL HERE
  #if model_name == "3DResNet":
    #MODEL HERE
  #if model_name == "3DResNet":
    #MODEL HERE

  if gpu_data_parallelize:
    gpus_list = multi_gpu.get_available_gpus(gpu_count)
    gpu_count = len(gpus_list)
    logger.info('INITIALIZING GPUs: {0}'.format(', '.join(gpus_list)))
    partype = 'dp'
    model = multi_gpu.make_parallel(model, gpus_list, partype=partype)
    logger.info(multi_gpu.print_mgpu_modelsummary(model))
    #model = multi_gpu_old.to_multi_gpu(model, gpu_count)

  if not multilabel:
    #model.compile(optimizer=Adam(lr=0.001, beta_1=0.9),loss='categorical_crossentropy', metrics=["accuracy"])
    model.compile(optimizer=SGD(lr=0.001, momentum=0.9, decay=0.0005, nesterov=True),loss='categorical_crossentropy', metrics=["accuracy"])
  else:
    #model.compile(optimizer=Adam(lr=0.001, beta_1=0.9),loss='binary_crossentropy', metrics=["accuracy"])
    model.compile(optimizer=SGD(lr=0.001, momentum=0.9, decay=0.0005, nesterov=True),loss='binary_crossentropy', metrics=["accuracy"])
  
  return model



