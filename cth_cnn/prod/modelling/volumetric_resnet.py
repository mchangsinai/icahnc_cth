import keras
from keras.models import Sequential, Model
from keras import layers
from keras.layers import Dense, Dropout, Activation, Flatten, Input, merge, Merge, Cropping3D, UpSampling3D, Embedding
from keras.layers import Conv3D, MaxPooling3D, BatchNormalization, AveragePooling3D, ZeroPadding3D, MaxPooling3D
from keras.optimizers import SGD, Adagrad, Adam


def identity_block(input_tensor, kernel_size, filters, stage, block):
  '''The identity_block is the block that has no conv layer at shortcut
  # Arguments
      input_tensor: input tensor
      kernel_size: defualt 3, the kernel size of middle conv layer at main path
      filters: list of integers, the nb_filters of 3 conv layer at main path
      stage: integer, current stage label, used for generating layer names
      block: 'a','b'..., current block label, used for generating layer names
  '''
  nb_filter1, nb_filter2, nb_filter3 = filters
  bn_axis = 4 #This is a necessary mod to make 3D b/c 3D and TF ordering

  conv_name_base = 'res' + str(stage) + block + '_branch'
  bn_name_base = 'bn' + str(stage) + block + '_branch'

  x = Conv3D(filters=nb_filter1, kernel_size=(1, 1, 1), name=conv_name_base + '2a')(input_tensor)
  x = BatchNormalization(axis=bn_axis, name=bn_name_base + '2a')(x)
  x = Activation('relu')(x)

  x = Conv3D(filters=nb_filter2, kernel_size=(kernel_size, kernel_size, kernel_size),
                    padding='same', name=conv_name_base + '2b')(x)
  x = BatchNormalization(axis=bn_axis, name=bn_name_base + '2b')(x)
  x = Activation('relu')(x)

  x = Conv3D(filters=nb_filter3, kernel_size=(1, 1, 1), name=conv_name_base + '2c')(x)
  x = BatchNormalization(axis=bn_axis, name=bn_name_base + '2c')(x)

  x = layers.add([x, input_tensor])
  x = Activation('relu')(x)
  return x


def conv_block(input_tensor, kernel_size, filters, stage, block, strides=(2, 2, 2)):
  '''conv_block is the block that has a conv layer at shortcut
  # Arguments
      input_tensor: input tensor
      kernel_size: defualt 3, the kernel size of middle conv layer at main path
      filters: list of integers, the nb_filters of 3 conv layer at main path
      stage: integer, current stage label, used for generating layer names
      block: 'a','b'..., current block label, used for generating layer names
  Note that from stage 3, the first conv layer at main path is with subsample=(2,2)
  And the shortcut should have subsample=(2,2) as well
  '''
  nb_filter1, nb_filter2, nb_filter3 = filters
  bn_axis = 4 #b/c 3D and TF ordering

  conv_name_base = 'res' + str(stage) + block + '_branch'
  bn_name_base = 'bn' + str(stage) + block + '_branch'

  x = Conv3D(filters=nb_filter1, kernel_size=(1, 1, 1), strides=strides,
                    name=conv_name_base + '2a')(input_tensor)
  x = BatchNormalization(axis=bn_axis, name=bn_name_base + '2a')(x)
  x = Activation('relu')(x)

  x = Conv3D(filters=nb_filter2, kernel_size=(kernel_size, kernel_size, kernel_size), padding='same',
                    name=conv_name_base + '2b')(x)
  x = BatchNormalization(axis=bn_axis, name=bn_name_base + '2b')(x)
  x = Activation('relu')(x)

  x = Conv3D(filters=nb_filter3, kernel_size=(1, 1, 1), name=conv_name_base + '2c')(x)
  x = BatchNormalization(axis=bn_axis, name=bn_name_base + '2c')(x)

  shortcut = Conv3D(filters=nb_filter3, kernel_size=(1, 1, 1), strides=strides,
                           name=conv_name_base + '1')(input_tensor)
  shortcut = BatchNormalization(axis=bn_axis, name=bn_name_base + '1')(shortcut)

  x = layers.add([x, shortcut])
  x = Activation('relu')(x)
  return x


def volresnet(num_classes, input_shape, downsample, multilabel):
  '''Instantiate the ResNet50 architecture,
  optionally loading weights pre-trained
  on ImageNet. Note that when using TensorFlow,
  for best performance you should set
  `image_dim_ordering="tf"` in your Keras config
  at ~/.keras/keras.json.
  The model and the weights are compatible with both
  TensorFlow and Theano. The dimension ordering
  convention used by the model is the one
  specified in your Keras config file.
  # Arguments
    include_top: whether to include the 3 fully-connected
        layers at the top of the network.
    weights: one of `None` (random initialization)
        or "imagenet" (pre-training on ImageNet).
    input_tensor: optional Keras tensor (i.e. xput of `layers.Input()`)
        to use as image input for the model.
  # Returns
    A Keras model instance.
  '''
  bn_axis = 4 #b/c 3D and TF ordering

  vol_input = Input(shape=input_shape)
  x = ZeroPadding3D(padding=(3, 3, 3))(vol_input)
  x = Conv3D(filters=64, kernel_size=(7, 7, 7), strides=(2, 2, 2), name='conv1')(x)
  x = BatchNormalization(axis=bn_axis, name='bn_conv1')(x)
  x = Activation('relu')(x)
  x = MaxPooling3D((3, 3, 3), strides=(2, 2, 2))(x)

  x = conv_block(x, 3, [64, 64, 256], stage=2, block='a')
  x = identity_block(x, 3, [64, 64, 256], stage=2, block='b')
  x = identity_block(x, 3, [64, 64, 256], stage=2, block='c')

  x = conv_block(x, 3, [128, 128, 512], stage=3, block='a')
  x = identity_block(x, 3, [128, 128, 512], stage=3, block='b')
  x = identity_block(x, 3, [128, 128, 512], stage=3, block='c')
  x = identity_block(x, 3, [128, 128, 512], stage=3, block='d')

  x = conv_block(x, 3, [256, 256, 1024], stage=4, block='a')
  x = identity_block(x, 3, [256, 256, 1024], stage=4, block='b')
  x = identity_block(x, 3, [256, 256, 1024], stage=4, block='c')
  x = identity_block(x, 3, [256, 256, 1024], stage=4, block='d')
  x = identity_block(x, 3, [256, 256, 1024], stage=4, block='e')
  x = identity_block(x, 3, [256, 256, 1024], stage=4, block='f')

  x = conv_block(x, 3, [512, 512, 2048], stage=5, block='a')
  x = identity_block(x, 3, [512, 512, 2048], stage=5, block='b')
  x = identity_block(x, 3, [512, 512, 2048], stage=5, block='c')
  #TODO:fiddle with this final downsampling... natively is (7,7)
  x = AveragePooling3D((2, 2, 2), name='avg_pool')(x)
  x = Flatten()(x)
  if not multilabel:
    classes = Dense(num_classes, activation='softmax', name='fc1000')(x)
  else:
    classes = Dense(num_classes, activation='sigmoid', name='fc1000')(x)
  model = Model(inputs=vol_input, outputs=classes)

  return model


def volumetric_resnet25(num_classes, input_shape, downsample, multilabel):
  '''Instantiate the ResNet50 architecture,
  optionally loading weights pre-trained
  on ImageNet. Note that when using TensorFlow,
  for best performance you should set
  `image_dim_ordering="tf"` in your Keras config
  at ~/.keras/keras.json.
  The model and the weights are compatible with both
  TensorFlow and Theano. The dimension ordering
  convention used by the model is the one
  specified in your Keras config file.
  # Arguments
    include_top: whether to include the 3 fully-connected
        layers at the top of the network.
    weights: one of `None` (random initialization)
        or "imagenet" (pre-training on ImageNet).
    input_tensor: optional Keras tensor (i.e. xput of `layers.Input()`)
        to use as image input for the model.
  # Returns
    A Keras model instance.
  '''
  bn_axis = 4 #b/c 3D and TF ordering

  vol_input = Input(shape=input_shape)
  #TODO(eko): I had to add an average pooling step at the beginning to downsample... still not enough memory :( :( :( 
  if downsample:
    x = AveragePooling3D((2,4,4), name="avg_pool1")(vol_input)
  else:
    x = (vol_input)
  x = ZeroPadding3D((3, 3, 3))(x)
  x = Conv3D(filters=64, kernel_size=(3, 3, 3), strides=(1, 2, 2), name='conv1')(x) #assymetric stride
  x = BatchNormalization(axis=bn_axis, name='bn_conv1')(x)
  x = Activation('relu')(x)
  # x = MaxPooling3D((3, 3, 3), strides=(2, 2, 2))(x)

  x = conv_block(x, 3, [64, 64, 256], stage=2, block='a', strides=(1, 2, 2))
  x = identity_block(x, 3, [64, 64, 256], stage=2, block='b')
  # x = identity_block(x, 3, [64, 64, 256], stage=2, block='c')

  x = conv_block(x, 3, [128, 128, 512], stage=3, block='a', strides=(1, 2, 2))
  x = identity_block(x, 3, [128, 128, 512], stage=3, block='b')
  # x = identity_block(x, 3, [128, 128, 512], stage=3, block='c')
  # x = identity_block(x, 3, [128, 128, 512], stage=3, block='d')

  x = conv_block(x, 3, [256, 256, 1024], stage=4, block='a', strides=(1, 2, 2))
  x = identity_block(x, 3, [256, 256, 1024], stage=4, block='b')
  # x = identity_block(x, 3, [256, 256, 1024], stage=4, block='c')
  # x = identity_block(x, 3, [256, 256, 1024], stage=4, block='d')
  # x = identity_block(x, 3, [256, 256, 1024], stage=4, block='e')
  # x = identity_block(x, 3, [256, 256, 1024], stage=4, block='f')

  # x = conv_block(x, 3, [512, 512, 2048], stage=5, block='a', strides=(2, 2, 2))
  # x = identity_block(x, 3, [512, 512, 2048], stage=5, block='b')
  # x = identity_block(x, 3, [512, 512, 2048], stage=5, block='c')
  # TODO:fiddle with this final downsampling... natively is (7,7)
  #x = AveragePooling3D((7, 7, 7), name='avg_pool')(x)
  x = AveragePooling3D((3, 3, 3), name='avg_pool')(x)
  x = Flatten()(x)
  if not multilabel:
    classes = Dense(num_classes, activation='softmax', name='fc1000')(x)
  else:
    classes = Dense(num_classes, activation='sigmoid', name='fc1000')(x)
  model = Model(inputs=vol_input, outputs=classes)

  return model


def mm_volumetric_resnet50(num_classes, input_shape, downsample, multilabel):
  '''
  multimodal version
  '''
  bn_axis = 4 #b/c 3D and TF ordering

  vol_input = Input(shape=input_shape)
  #TODO(eko): I had to add an average pooling step at the beginning to downsample... still not enough memory :( :( :( 
  if downsample:
    x = AveragePooling3D((1,2,2), name="avg_pool1")(vol_input)
  else:
    x = (vol_input)
  x = ZeroPadding3D((3, 3, 3))(x)
  x = Conv3D(filters=64, kernel_size=(7, 7, 7), strides=(2, 2, 2), name='conv1')(x)
  x = BatchNormalization(axis=bn_axis, name='bn_conv1')(x)
  x = Activation('relu')(x)
  x = MaxPooling3D((3, 3, 3), strides=(2, 2, 2))(x)

  x = conv_block(x, 3, [64, 64, 256], stage=2, block='a', strides=(1, 1, 1))
  x = identity_block(x, 3, [64, 64, 256], stage=2, block='b')
  x = identity_block(x, 3, [64, 64, 256], stage=2, block='c')

  x = conv_block(x, 3, [128, 128, 512], stage=3, block='a')
  x = identity_block(x, 3, [128, 128, 512], stage=3, block='b')
  x = identity_block(x, 3, [128, 128, 512], stage=3, block='c')
  x = identity_block(x, 3, [128, 128, 512], stage=3, block='d')

  x = conv_block(x, 3, [256, 256, 1024], stage=4, block='a')
  x = identity_block(x, 3, [256, 256, 1024], stage=4, block='b')
  x = identity_block(x, 3, [256, 256, 1024], stage=4, block='c')
  x = identity_block(x, 3, [256, 256, 1024], stage=4, block='d')
  x = identity_block(x, 3, [256, 256, 1024], stage=4, block='e')
  x = identity_block(x, 3, [256, 256, 1024], stage=4, block='f')

  x = conv_block(x, 3, [512, 512, 2048], stage=5, block='a')
  x = identity_block(x, 3, [512, 512, 2048], stage=5, block='b')
  x = identity_block(x, 3, [512, 512, 2048], stage=5, block='c')
  #TODO:fiddle with this final downsampling... natively is (7,7)
  x = AveragePooling3D((2, 8, 8), name='avg_pool')(x)
  x_img = Flatten()(x)

  #introduce and embed auxillary inputs
  age_input = Input(shape=(1,), name="age_input")
  gen_input = Input(shape=(1,), name="gen_input")
  x_age = Embedding(output_dim=10, input_dim=100, input_length=1)(age_input)
  x_age = Flatten()(x_age)
  x_gen = Embedding(output_dim=10, input_dim=2, input_length=1)(gen_input)
  x_gen = Flatten()(x_gen)
  #concatenate aux_inputs to main image input
  x = layers.concatenate([x_img, x_age, x_gen])
  #get preds
  if not multilabel:
    classes = Dense(num_classes, activation='softmax', name='fc1000')(x)
  else:
    classes = Dense(num_classes, activation='sigmoid', name='fc1000')(x)
  model = Model(inputs=[vol_input, gen_input, age_input], outputs=classes)

  return model


def volumetric_resnet101(num_classes, input_shape):
  '''Instantiate the ResNet50 architecture,
  optionally loading weights pre-trained
  on ImageNet. Note that when using TensorFlow,
  for best performance you should set
  `image_dim_ordering="tf"` in your Keras config
  at ~/.keras/keras.json.
  The model and the weights are compatible with both
  TensorFlow and Theano. The dimension ordering
  convention used by the model is the one
  specified in your Keras config file.
  # Arguments
    include_top: whether to include the 3 fully-connected
        layers at the top of the network.
    weights: one of `None` (random initialization)
        or "imagenet" (pre-training on ImageNet).
    input_tensor: optional Keras tensor (i.e. xput of `layers.Input()`)
        to use as image input for the model.
  # Returns
    A Keras model instance.
  '''
  bn_axis = 4 #b/c 3D and TF ordering

  vol_input = Input(shape=input_shape)
  #TODO(eko): I had to add an average pooling step at the beginning to downsample... still not enough memory :( :( :( 
  x = AveragePooling3D((1,2,2), name="avg_pool1")(vol_input)
  x = ZeroPadding3D((3, 3, 3))(x)
  x = Conv3D(64, 7, 7, 7, subsample=(2, 2, 2), name='conv1')(x)
  x = BatchNormalization(axis=bn_axis, name='bn_conv1')(x)
  x = Activation('relu')(x)
  x = MaxPooling3D((3, 3, 3), strides=(2, 2, 2))(x)

  x = conv_block(x, 3, [64, 64, 256], stage=2, block='a', strides=(1, 1, 1))
  x = identity_block(x, 3, [64, 64, 256], stage=2, block='b')
  x = identity_block(x, 3, [64, 64, 256], stage=2, block='c')

  x = conv_block(x, 3, [128, 128, 512], stage=3, block='a')
  x = identity_block(x, 3, [128, 128, 512], stage=3, block='b')
  x = identity_block(x, 3, [128, 128, 512], stage=3, block='c')
  x = identity_block(x, 3, [128, 128, 512], stage=3, block='d')

  x = conv_block(x, 3, [256, 256, 1024], stage=4, block='a')
  x = identity_block(x, 3, [256, 256, 1024], stage=4, block='b')
  x = identity_block(x, 3, [256, 256, 1024], stage=4, block='c')
  x = identity_block(x, 3, [256, 256, 1024], stage=4, block='d')
  x = identity_block(x, 3, [256, 256, 1024], stage=4, block='e')
  x = identity_block(x, 3, [256, 256, 1024], stage=4, block='f')
  x = identity_block(x, 3, [256, 256, 1024], stage=4, block='g')
  x = identity_block(x, 3, [256, 256, 1024], stage=4, block='h')
  x = identity_block(x, 3, [256, 256, 1024], stage=4, block='i')
  x = identity_block(x, 3, [256, 256, 1024], stage=4, block='j')
  x = identity_block(x, 3, [256, 256, 1024], stage=4, block='k')
  x = identity_block(x, 3, [256, 256, 1024], stage=4, block='l')
  x = identity_block(x, 3, [256, 256, 1024], stage=4, block='m')
  x = identity_block(x, 3, [256, 256, 1024], stage=4, block='n')
  x = identity_block(x, 3, [256, 256, 1024], stage=4, block='o')
  x = identity_block(x, 3, [256, 256, 1024], stage=4, block='p')
  x = identity_block(x, 3, [256, 256, 1024], stage=4, block='q')
  x = identity_block(x, 3, [256, 256, 1024], stage=4, block='r')
  x = identity_block(x, 3, [256, 256, 1024], stage=4, block='s')
  x = identity_block(x, 3, [256, 256, 1024], stage=4, block='t')
  x = identity_block(x, 3, [256, 256, 1024], stage=4, block='u')
  x = identity_block(x, 3, [256, 256, 1024], stage=4, block='v')
  x = identity_block(x, 3, [256, 256, 1024], stage=4, block='w')

  x = conv_block(x, 3, [512, 512, 2048], stage=5, block='a')
  x = identity_block(x, 3, [512, 512, 2048], stage=5, block='b')
  x = identity_block(x, 3, [512, 512, 2048], stage=5, block='c')
  #only pool by 1 along depth axis b/c fewer slices
  x = AveragePooling3D((1, 7, 7), name='avg_pool')(x)
  x = Flatten()(x)
  classes = Dense(num_classes, activation='softmax', name='fc1000')(x)

  model = Model(input=vol_input, output=classes)

  return model