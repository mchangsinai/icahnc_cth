"""
LICENSING:
Copyright 2016, Mount Sinai Health System. All rights reserved.

This software is part of the ICAHNC group source code in the Mount Sinai Health System (MSHS).
Redistribution and use in source and binary forms, with or without modification, are not permitted 
without the explicit and written consent of the Principal Investigators (EKO, JT) or designated
representatives.

ICAHNC DEV TEAM:
Eric Karl Oermann, M.D.
Marcus Badgeley, M.Eng.

LIBRARY SUMMARY:
***
"""

import keras
from keras.utils.np_utils import to_categorical
from keras.utils.generic_utils import Progbar
from sklearn.metrics import roc_auc_score
from sklearn.metrics import average_precision_score
from scipy.ndimage import interpolation
from glob import glob
#custom imports
from cth_3dcnn import generate_augmented_arrays_from_file, log_architecture, get_metadata
from cth_models import model_builder

import numpy as np
import pandas as pd
import json
import os, sys

import time
from random import shuffle
import io
import zlib
from contextlib import redirect_stdout

import logging
logger = logging.getLogger()
logger.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
fh = logging.FileHandler('/home/eko/projects/icahnc_cth/cth_cnn/logs/cth_3dcnn_test.log')
fh.setLevel(logging.DEBUG)
fh.setFormatter(formatter)
logger.addHandler(fh)
# ch = logging.StreamHandler()
# ch.setLevel(logging.DEBUG)
# ch.setFormatter(formatter)
# logger.addHandler(ch)

#import all of the methods from the training module


def predict_test_results(model_name=None, downsample=False, epochs=10, batch_size=32, frxn_of_epoch_per_log=1, augment=False, skull_strip=False, blood_highlight=False,
  rotate=False, training_data_path=None, test_data_path=None, main_dir=None, multilabel=False, source_type=None, gpu_data_parallelize=False, gpu_count=1,
  workers=1, pickle_safe=False, resume_last_ckpt=False, ckpt_file=None, train_test=None):
  """
  TODO(eko): add docstring
  """
  logger.info("==============NEW INFERENCE STARTING==============")
  
   #load metadata 
  num_classes, input_shape, nb_train, _, cost_weights = get_metadata(training_data_path)
  _, _, nb_test, _, _ = get_metadata(test_data_path)

  #Load name
  exp_name = "M_{0}_Yn{1}_Bn{2}_A_{3}_GPU{4}".format(model_name,num_classes,batch_size,augment,gpu_count)
  model_dir = main_dir+"/models"
  logger.info(exp_name)
 
  if resume_last_ckpt:
    logger.info("ATTEMPTING TO RESTORE FROM CHECKPOINT")
    try:
      if ckpt_file:
        ckpt_file=os.path.join(model_dir,ckpt_file)
      else:
        #if we manually pass one, use that, otherwise load the farthest one by steps
        ckpt_file = sorted(glob("{0}/{1}*".format(model_dir,exp_name)))[-1]
      model = keras.models.load_model(ckpt_file)
      logger.info("RESTORATION SUCCESSFUL")
      logger.info("LOADED:{0}".format(ckpt_file))
    except:
      logger.info("RESTORATION FAILURE")
      model = model_builder(
        model_name=model_name,
        num_classes=num_classes,
        input_shape=input_shape,
        multilabel=False,
        gpu_data_parallelize=gpu_data_parallelize,
        gpu_count=gpu_count,
        downsample=downsample,
        source_type=source_type)
  else:
    model = model_builder(
      model_name=model_name,
      num_classes=num_classes,
      input_shape=input_shape,
      multilabel=False,
      gpu_data_parallelize=gpu_data_parallelize,
      gpu_count=gpu_count,
      downsample=downsample,
      source_type=source_type)

  test_generator = generate_augmented_arrays_from_file(
    path=test_data_path,
    source_type=source_type, #can make json if we don't want metadata, but need to change below
    num_classes=num_classes,
    batch_size=batch_size,
    augment=False,
    skull_strip=skull_strip,
    blood_highlight=blood_highlight,
    rotate=rotate)

  test_count = 0
  y_true_list = []
  y_pred_list = []
  gen_list = []
  age_list = []
  service_list = []
  physician_list = []
  logger.info("CALCULATING VALIDATION ROC")
  progress_bar = Progbar(target=nb_test)
  while test_count<nb_test:
    progress_bar.update(test_count)
    ([img, gen, age, service, physician], label) = next(test_generator)
    y_true_list.append(label)
    gen_list.append(gen)
    age_list.append(age)
    service_list.append(service)
    physician_list.append(physician)
    y_pred_list.append(model.predict_on_batch(img))
    test_count+=batch_size
  y_true = np.vstack(y_true_list)
  y_pred = np.vstack(y_pred_list)
  y_gen = np.vstack(gen_list)
  y_age = np.vstack(age_list)
  y_service = np.vstack(service_list)
  y_physician = np.vstack(physician_list)
  #TODO(eko): implement slicker way of converting out of one-hot to categorical here
  val_roc_auc = roc_auc_score(y_true[:,1], y_pred[:,1])
  pr_roc_auc = average_precision_score(y_true[:,1], y_pred[:,1])
  logger.info("val_roc_auc:{0} | pr_roc_auc:{1}".format(val_roc_auc,pr_roc_auc))
  logger.info("SAVING VALIDATION PRED/TRUE AT: {0}".format(main_dir+"/results"))
  pd.to_pickle(path=main_dir+'/results/{0}_{1}-y_true'.format(exp_name,train_test),obj=y_true)
  pd.to_pickle(path=main_dir+'/results/{0}_{1}-y_pred'.format(exp_name,train_test),obj=y_pred)
  pd.to_pickle(path=main_dir+'/results/{0}_{1}-y_gen'.format(exp_name,train_test),obj=y_gen)
  pd.to_pickle(path=main_dir+'/results/{0}_{1}-y_age'.format(exp_name,train_test),obj=y_age)
  pd.to_pickle(path=main_dir+'/results/{0}_{1}-y_service'.format(exp_name,train_test),obj=y_service)
  pd.to_pickle(path=main_dir+'/results/{0}_{1}-y_physician'.format(exp_name,train_test),obj=y_physician)


if __name__ == "__main__":
  # PATH_TO_TRAIN = "/home/eko/projects/icahnc_cth/cth_cnn/data/labels/train_cth_labels_features.json"
  # PATH_TO_VAL = "/home/eko/projects/icahnc_cth/cth_cnn/data/labels/val_cth_labels_features.json"
  # NUM_EPOCHS = 1000
  # BATCH_SIZE = 2
  # MODEL_NAME = "voxnet"
  # MODEL_NAME = "3dresnet"
  # MODEL_NAME = "3dvgg16"
  # MODEL_NAME = "3dresnet_elu"
  # #save trained models here
  # MAIN_DIR = "/home/eko/projects/icahnc_cth/cth_cnn"

  #TODO(eko): UNCOMMENT THESE LINES IF YOU WANT TO RUN ON CPU ONLY
  # os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"   # see issue #152
  # os.environ["CUDA_VISIBLE_DEVICES"]=""

  """
  NOTES:
  1. frxn_of_epoch_per_log will make each epoch a frxn of the whole dataset. This will 
  lead to more logs being written and more models being saved, but also !a more rapid weight decay!
  2. 
  """
  CHECKPOINT_FILE = "2017_07_26_23_37_09_M_volresnet_Yn2_Bn4_A_True_GPU1_weights.07-0.52.hdf5"

  predict_test_results(
    model_name="volresnet",
    downsample=False,
    epochs=1000,
    batch_size=4,
    frxn_of_epoch_per_log=2,
    augment=False,
    skull_strip=False,
    blood_highlight=False,
    rotate=False,
    training_data_path="/home/eko/projects/icahnc_cth/cth_cnn/data/labels/train_cth_labels_features.json",
    test_data_path="/home/eko/projects/icahnc_cth/cth_cnn/data/labels/test_cth_labels_features_new.json",
    main_dir="/home/eko/projects/icahnc_cth/cth_cnn",
    multilabel=False,
    source_type="multimodal",
    gpu_count=1,
    workers=1,
    pickle_safe=False,
    resume_last_ckpt=True,
    ckpt_file=CHECKPOINT_FILE,
    train_test="test")
  
  print("GETTING VAL RESULTS")
  predict_test_results(
    model_name="volresnet",
    downsample=False,
    epochs=1000,
    batch_size=4,
    frxn_of_epoch_per_log=2,
    augment=False,
    skull_strip=False,
    blood_highlight=False,
    rotate=False,
    training_data_path="/home/eko/projects/icahnc_cth/cth_cnn/data/labels/val_cth_labels_features.json",
    test_data_path="/home/eko/projects/icahnc_cth/cth_cnn/data/labels/val_cth_labels_features.json",
    main_dir="/home/eko/projects/icahnc_cth/cth_cnn",
    multilabel=False,
    source_type="multimodal",
    gpu_data_parallelize=False,
    gpu_count=1,
    workers=1,
    pickle_safe=False,
    resume_last_ckpt=True,
    ckpt_file=CHECKPOINT_FILE,
    train_test="val")

  # print("GETTING TRAIN RESULTS")
  # predict_test_results(
  #   model_name="3dresnet",
  #   downsample=True,
  #   epochs=1000,
  #   batch_size=4,
  #   frxn_of_epoch_per_log=2,
  #   augment=True,
  #   skull_strip=False,
  #   blood_highlight=False,
  #   rotate=True,
  #   training_data_path="/home/eko/projects/icahnc_cth/cth_cnn/data/labels/train_cth_labels_features.json",
  #   test_data_path="/home/eko/projects/icahnc_cth/cth_cnn/data/labels/train_cth_labels_features.json",
  #   main_dir="/home/eko/projects/icahnc_cth/cth_cnn",
  #   multilabel=False,
  #   gpu_data_parallelize=False,
  #   gpu_count=1,
  #   workers=1,
  #   pickle_safe=False,
  #   resume_last_ckpt=True,
  #   ckpt_file="M_3dresnet_Yn2_Bn4_A_False_GPU1_weights.24-1.18.hdf5",
  #   train_test="train")
