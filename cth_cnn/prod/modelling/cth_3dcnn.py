"""
LICENSING:
Copyright 2016, Mount Sinai Health System. All rights reserved.

This software is part of the ICAHNC group source code in the Mount Sinai Health System (MSHS).
Redistribution and use in source and binary forms, with or without modification, are not permitted 
without the explicit and written consent of the Principal Investigators (EKO, JT) or designated
representatives.

ICAHNC DEV TEAM:
Eric Karl Oermann, M.D.
Marcus Badgeley, M.Eng.

LIBRARY SUMMARY:
***
"""

import keras
from keras.utils.np_utils import to_categorical
from keras.utils.generic_utils import Progbar
from sklearn.metrics import roc_auc_score
from sklearn.metrics import average_precision_score
from scipy.ndimage import interpolation
from glob import glob
from cth_models import model_builder

import numpy as np
import pandas as pd
import json
import os, sys

from time import gmtime, strftime
from random import shuffle
import io
import zlib
from contextlib import redirect_stdout

import logging
logger = logging.getLogger()
logger.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
fh = logging.FileHandler('/home/eko/projects/icahnc_cth/cth_cnn/logs/cth_3dcnn.log')
fh.setLevel(logging.DEBUG)
fh.setFormatter(formatter)
logger.addHandler(fh)

def log_architecture(model):
  with io.StringIO() as buf, redirect_stdout(buf):
    model.summary()
    output = buf.getvalue()
    logger.info(output)

def load_images(examples):
  img_list = []
  for example in examples:
    img_arr = np.load(example["filenames"])
    img_list.append(img_arr)
  img_arr = np.array(img_list)
  return img_arr

def load_metadata(examples):
  gen_list = []
  age_list = []
  service_list = []
  physician_list = []
  for example in examples:
    gen_list.append(np.array(example["patient_sex"]))
    age_list.append(np.array(example["patient_age"]))
    service_list.append(np.array(example["patient_service"]))
    physician_list.append(np.array(example["patient_physician"]))
  gen_arr = np.array(gen_list)[:,np.newaxis]
  age_arr = np.array(age_list)[:,np.newaxis]
  service_arr = np.array(service_list)[:,np.newaxis]
  physician_arr = np.array(physician_list)[:,np.newaxis]
  return gen_arr, age_arr, service_arr, physician_arr
  
def load_labels(examples, num_classes):
  label_list = []
  for example in examples:
    label = np.array(example["labels"])
    #label = to_categorical(label,num_classes)
    label_list.append(label)
  label_arr = np.array(label_list)
  return label_arr

def load_dir_vols(examples):
  vols=[]
  for i in examples:
    fileobj = open(i, 'rb')
    buf = zlib.decompress(fileobj.read())
    arr = np.load(io.BytesIO(buf))
    vols.append(arr)
  return np.array(vols)

def load_dir_lbls(examples):
  lbls = []
  for i in examples:
    buf = int(i.split("/")[-1].split(".")[0])-1
    buf = to_categorical(buf,10)
    lbls.append(buf)
  return np.array(lbls)


def augment_image_array(img=None, skull_strip=False, blood_highlight=False, rotate=False, downsample=False):
  """
  """
  img_list=[]
  if downsample:
    for i in range(img.shape[0]):
      img[i,:,:,:,:] = img[i,:,::2,::2,:] #interpolation.zoom(img[i,:,:,:,:], zoom=[1,2,2,0])
  if rotate:
    for i in range(img.shape[0]):
      rot, axes = get_rand_rots() 
      img[i,:,:,:,:] = interpolation.rotate(img[i,:,:,:,:], angle=rot, axes=axes, reshape=False, output=None, order=3, mode='constant', cval=0.0, prefilter=True)
  elif skull_strip | blood_highlight:
    for i in range(img.shape[0]):
      img_thresh = img[i]
      if skull_strip & blood_highlight:
        img_thresh_blood=((img_thresh <= 90) & (img_thresh >= 40)) * img_thresh
        img_thresh_bone=((img_thresh_blood <= 700)) * img_thresh_blood
        img_thresh = img_thresh_bone
      elif (not skull_strip) & blood_highlight:
        img_thresh = ((img_thresh <= 90) & (img_thresh >= 40)) * img_thresh
      elif skull_strip & (not blood_highlight):
        img_thresh = ((img_thresh <= 200)) * img_thresh
      img_list.append(img_thresh)
    img = np.array(img_list)
  return img


def get_rand_rots():
  """
  z-axis rotation... 2/3 of the time it just wiggles a little bit, 1/3 it gets a spin from 0-360.
  """
  randaxis = np.random.randint(low=0,high=2)
  if randaxis == 0:
    rot = np.random.randint(low=0, high=180, size=None, dtype='l')
    axes = (2, 1)
  elif randaxis == 1:
    rot = np.random.randint(low=0, high=180, size=None, dtype='l')
    axes = (2, 1)
  elif randaxis == 2:
    rot = np.random.randint(low=0, high=180, size=None, dtype='l')
    axes = (2, 1)
  return rot, axes


def generate_augmented_arrays_from_file(path=None, source_type="json", num_classes=2,
  batch_size=2, augment=False, skull_strip=False, blood_highlight=False, rotate=False, downsample=False):
  """
  Note that for augmentation, only the dir source_type for the ModelNet10 directory has rotational augmentation
  """
  if source_type == "dir":  
    while 1:
      data_files = [os.path.join(path, i) for i in os.listdir(path)]
      shuffle(data_files)
      sample_size = len(data_files)
      for i in range(0,sample_size,batch_size):
        examples = data_files[i:i+batch_size]
        vols = load_dir_vols(examples)

        if augment:
          #if we're going to augment, take all of the examples and rotate them by some random number around the z-axis
          for i in range(vols.shape[0]):
            rot, axes = get_rand_rots() 
            vols[i,:,:,:] = rotate(vols[i,:,:,:], angle=rot, axes=axes, reshape=False, output=None, order=3, mode='constant', cval=0.0, prefilter=True)

        vols = vols[:,:,:,:,np.newaxis]
        labels = load_dir_lbls(examples)
        labels = np.squeeze(labels)
        yield (vols, labels)   
  if source_type == "json":  
    while 1:
      with open(path) as json_file:
        data = json.load(json_file)
        sample_size = len(data["examples"])
        for i in range(0,sample_size,batch_size):
          #TODO(ekoermann): shouldn't need exception handling... what array is fucking me here?
          #ACHTUNG! ACHTUNG! ACHTUNG! ACHTUNG! ACHTUNG! ACHTUNG!
          try:
            examples = data["examples"][i:i+batch_size]
            # create numpy arrays of input data
            # and labels, from each line in the file
            img = load_images(examples)
            #these two lines were a hack for when I messed up axes
            img = np.squeeze(img)
            img = img[:,:,:,:,np.newaxis]
            label = load_labels(examples,num_classes)
            #THIS IS ANOTHER HACK FOR THE WRONG DIM ON LABEL...
            label = np.squeeze(label)

            if augment:
              img = augment_image_array(img, skull_strip, blood_highlight, rotate, downsample) #TODO(eko) add in downsample call from train

            yield (img, label)
          except IndexError:
            logger.error(str(examples))
            continue
          except ValueError:
            logger.error(str(examples))
            continue
  if source_type == "multimodal":  
    while 1:
      with open(path) as json_file:
        data = json.load(json_file)
        sample_size = len(data["examples"])
        for i in range(0,sample_size,batch_size):
          #TODO(ekoermann): shouldn't need exception handling... what array is fucking me here?
          #ACHTUNG! ACHTUNG! ACHTUNG! ACHTUNG! ACHTUNG! ACHTUNG!
          try:
            examples = data["examples"][i:i+batch_size]
            # create numpy arrays of input data
            # and labels, from each line in the file
            img = load_images(examples)
            gen, age, service, physician = load_metadata(examples)
            #these two lines were a hack for when I messed up axes
            img = np.squeeze(img)
            img = img[:,:,:,:,np.newaxis]
            label = load_labels(examples,num_classes)
            #THIS IS ANOTHER HACK FOR THE WRONG DIM ON LABEL...
            label = np.squeeze(label)

            if augment:
              img = augment_image_array(img, skull_strip, blood_highlight, rotate)

            yield ([img, gen, age, service, physician], label)
          except IndexError:
            logger.error(str(examples))
            print(img.shape)
            continue
          except ValueError:
            logger.error(str(examples))
            print(img.shape)
            continue
   

def get_metadata(path):
  """
  Helper method to pull the metadata from the json files at train time
  Args:
    path: path to the json file holding the data
  Returns:
    num_classes: the number of classes in the given dataset
    input_shape: the shape of the arrays that the given dataset is pulling from usually (40,256,256)
  """
  with open(path) as json_file:
    print("retrieving metadata")
    data = json.load(json_file)
    num_classes = data["metadata"]["num_classes"]
    input_shape = data["metadata"]["input_shape"]
    num_examples = data["metadata"]["num_examples"]
    target_classes = data["metadata"]["target_classes"]
    cost_weights = data["metadata"]["cost_weights"]
    cost_weights = {int(k):v for k,v in cost_weights.items()}
  return num_classes, input_shape, num_examples, target_classes, cost_weights


def train(model_name=None, downsample=False, epochs=10, batch_size=32, frxn_of_epoch_per_log=1, augment=False, skull_strip=False, blood_highlight=False,
  rotate=False, training_data_path=None, validation_data_path=None, main_dir=None,
  multilabel=False, source_type=None, gpu_data_parallelize=False, gpu_count=1,
  workers=1, pickle_safe=False, resume_last_ckpt=False, ckpt_file=None):
  """
  TODO(eko): add docstring
  """
  run_start_time = strftime("%Y_%m_%d_%H_%M_%S", gmtime())
  logger.info("==============NEW EXPERIMENT STARTING==============")
  logger.info("START TIME:{0}".format(run_start_time))
  
   #load metadata 
  num_classes, input_shape, nb_train, _, cost_weights = get_metadata(training_data_path)
  _, _, nb_val, _, _ = get_metadata(validation_data_path)

  # #adjust batch size for multiple gpus:
  # batch_size = batch_size * gpu_count

  #Load name
  exp_name = "{0}_M_{1}_Yn{2}_Bn{3}_A_{4}_GPU{5}".format(run_start_time,model_name,num_classes,batch_size,augment,gpu_count)
  model_dir = main_dir+"/models"
  logger.info(exp_name)
  logger.info("SAVING MODELS TO {0}".format(model_dir))
  logger.info("BUILDING A NEW {0} TYPE MODEL".format(source_type))
 
  if resume_last_ckpt:
    logger.info("ATTEMPTING TO RESTORE FROM CHECKPOINT")
    try:
      if ckpt_file:
        ckpt_file=os.path.join(model_dir,ckpt_file)
      else:
        #if we manually pass one, use that, otherwise load the farthest one by steps
        ckpt_file = sorted(glob("{0}/{1}*".format(model_dir,exp_name)))[-1]
      model = keras.models.load_model(ckpt_file)
      logger.info("RESTORATION SUCCESSFUL")
      logger.info("LOADED:{0}".format(ckpt_file))
    except:
      logger.info("RESTORATION FAILURE")
      model = model_builder(
        model_name=model_name,
        num_classes=num_classes,
        input_shape=input_shape,
        multilabel=multilabel,
        gpu_data_parallelize=gpu_data_parallelize,
        gpu_count=gpu_count,
        downsample=downsample,
        source_type=source_type)
  else:
    logger.info("BUILDING MODEL FROM SCRATCH")
    model = model_builder(
      model_name=model_name,
      num_classes=num_classes,
      input_shape=input_shape,
      multilabel=multilabel,
      gpu_data_parallelize=gpu_data_parallelize,
      gpu_count=gpu_count,
      downsample=downsample,
      source_type=source_type)

  #write model architecture to log
  log_architecture(model)

  #callbacks for the training step
  callbacks = []
  #initialize tensorboard. histo frequency sets how frequently to calc histo of activation weights
  
  tensorboard = keras.callbacks.TensorBoard(
    log_dir=main_dir+'/logs/'+exp_name,
    histogram_freq=1,
    write_graph=False,
    write_images=False)
  callbacks.append(tensorboard)

  #initialize checkpointing. "period" sets the checkpointing frequency
  checkpointer = keras.callbacks.ModelCheckpoint(
    filepath=model_dir+"/"+exp_name+"_weights.{epoch:02d}-{val_loss:.2f}.hdf5",
    monitor='val_loss',
    verbose=0,
    save_best_only=False,
    save_weights_only=False,
    mode='auto',
    period=1)
  callbacks.append(checkpointer)

  if not multilabel:
    logger.info("FITTING BINARY RELEVANCE MODEL")
    #each epoch is, traditionally, an entire run through on the dataset... will shrink so can log better detail
    hist = model.fit_generator(
      generator=generate_augmented_arrays_from_file(
        path=training_data_path,
        source_type=source_type,
        num_classes=num_classes,
        batch_size=batch_size,
        augment=augment,
        skull_strip=skull_strip,
        blood_highlight=blood_highlight,
        rotate=rotate,
        downsample=downsample),
      steps_per_epoch=(nb_train//batch_size),
      epochs=epochs,
      callbacks=callbacks,
      validation_data = generate_augmented_arrays_from_file(
        path=validation_data_path,
        source_type=source_type,
        num_classes=num_classes,
        batch_size=batch_size,
        augment=False,
        skull_strip=skull_strip,
        blood_highlight=blood_highlight,
        rotate=rotate,
        downsample=downsample),
      validation_steps=nb_val,
      workers=workers, #Workers for multithreading generator
      pickle_safe=pickle_safe) #whether to pickle this bad boy and multithread
  else:
    logger.info("FITTING MULTILABEL MODEL WITH PER-CLASS COST WEIGHTS")
    hist = model.fit_generator(
      generator=generate_augmented_arrays_from_file(
        path=training_data_path,
        source_type=source_type,
        num_classes=num_classes,
        batch_size=batch_size,
        augment=augment,
        skull_strip=skull_strip,
        blood_highlight=blood_highlight,
        rotate=rotate),
      steps_per_epoch=(nb_train//batch_size)//frxn_of_epoch_per_log,
      epochs=epochs,
      callbacks=callbacks,
      validation_data = generate_augmented_arrays_from_file(
        path=validation_data_path,
        source_type=source_type,
        num_classes=num_classes,
        batch_size=batch_size,
        augment=False,
        skull_strip=skull_strip,
        blood_highlight=blood_highlight,
        rotate=rotate),
      validation_steps=nb_val,
      class_weight=cost_weights,
      workers=workers, #Workers for multithreading generator
      pickle_safe=pickle_safe) #whether to pickle this bad boy and multithread


  # calculate AUC for the validation set in a batch-wise manner and then push everything into a dataframe and save
  # as a .csv for later use

  test_generator = generate_augmented_arrays_from_file(
    path=validation_data_path,
    source_type=source_type,
    num_classes=num_classes,
    batch_size=batch_size,
    augment=False,
    skull_strip=skull_strip,
    blood_highlight=blood_highlight)

  test_count = 0
  y_true_list = []
  y_pred_list = []
  print("CALCULATING VALIDATION ROC")
  progress_bar = Progbar(target=nb_val)
  while test_count<nb_val:
    progress_bar.update(test_count)
    (img,lbl) = next(test_generator)
    y_true_list.append(lbl)
    y_pred_list.append(model.predict_on_batch(img))
    test_count+=batch_size
  y_true = np.vstack(y_true_list)
  y_pred = np.vstack(y_pred_list)
  #nb_val
  val_roc_auc = roc_auc_score(y_true, y_pred)
  pr_roc_auc = average_precision_score(y_true, y_pred)

  #hacky, but we multiply AUC by the length of val_acc just so it has as many entries as others and wil fit in dataframe
  history_results = {
    "train_loss" : hist.history["loss"],
    "train_acc" : hist.history["acc"],
    "val_loss" : hist.history["val_loss"],
    "val_acc" : hist.history["val_acc"],
    "tot_val_ROCAUC" : [val_roc_auc]*len(hist.history["val_acc"]),
    "tot_val_PRAUC": [pr_roc_auc]*len(hist.history["val_acc"])}

  df_history = pd.DataFrame(history_results)
  df_history.to_csv(main_dir+'/results/{0}-history.csv'.format(exp_name))
  np.savetxt(main_dir+'/results/{0}-y_true.csv'.format(exp_name),y_true,delimiter=",")
  np.savetxt(main_dir+'/results/{0}-y_pred.csv'.format(exp_name),y_pred,delimiter=",")
   
  #save model
  model.save(main_dir+"/models/"+exp_name)


if __name__ == "__main__":
  """
  # PATH_TO_TRAIN = "/home/eko/projects/icahnc_cth/cth_cnn/data/labels/train_cth_labels_features.json"
  # PATH_TO_VAL = "/home/eko/projects/icahnc_cth/cth_cnn/data/labels/val_cth_labels_features.json"
  # NUM_EPOCHS = 1000
  # BATCH_SIZE = 2
  # MODEL_NAME = "voxnet"
  # MODEL_NAME = "3dresnet"
  # MODEL_NAME = "3dvgg16"
  # MODEL_NAME = "3dresnet_elu"
  # MODEL_NAME = "volumetric_resnet25" #this is my latest one that is wider and narrower
  # #save trained models here
  # MAIN_DIR = "/home/eko/projects/icahnc_cth/cth_cnn"

  #TODO(eko): UNCOMMENT THESE LINES IF YOU WANT TO RUN ON CPU ONLY
  # os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"   # see issue #152
  # os.environ["CUDA_VISIBLE_DEVICES"]=""

  
  NOTES:
  1. frxn_of_epoch_per_log will make each epoch a frxn of the whole dataset. This will 
  lead to more logs being written and more models being saved, but also !a more rapid weight decay!
  2. source_type = "json" for just images or "multimodal" for multimodal
  """

  train(
    model_name="volresnet", #3resnet is my go to... try multimodal sometime
    downsample=False,
    epochs=1000,
    batch_size=4, #adjust for num GPUs... this is TOTAL batch size, it will be divided per GPU
    frxn_of_epoch_per_log=1,
    augment=True,
    skull_strip=False,
    blood_highlight=False,
    rotate=True,
    training_data_path="/home/eko/projects/icahnc_cth/cth_cnn/data/labels/train_cth_labels_features.json",
    validation_data_path="/home/eko/projects/icahnc_cth/cth_cnn/data/labels/val_cth_labels_features.json",
    main_dir="/home/eko/projects/icahnc_cth/cth_cnn",
    multilabel=False,
    source_type="json",
    gpu_data_parallelize=False,
    gpu_count=1,
    workers=1,
    pickle_safe=False,
    resume_last_ckpt=False,
    ckpt_file="2017_08_22_22_25_18_M_volresnet_Yn2_Bn4_A_True_GPU1_weights.03-0.63.hdf5")