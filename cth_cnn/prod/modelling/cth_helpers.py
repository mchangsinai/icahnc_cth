"""
LICENSING:
Copyright 2016, Mount Sinai Health System. All rights reserved.

This software is part of the ICAHNC group source code in the Mount Sinai Health System (MSHS).
Redistribution and use in source and binary forms, with or without modification, are not permitted 
without the explicit and written consent of the ICAHNC Principal Investigators (EKO, JT) or designated
representatives.

ICAHNC DEV TEAM:
Eric Karl Oermann, M.D.
Marcus Badgeley, M.Eng.

LIBRARY SUMMARY:
***
"""

import numpy as np
import pylab
import matplotlib.pyplot as plt
#CELL FOR DISPLAYING CONTENTS OF SLICES AND SAVING A MERGED ONE TO THE RESULTS FOLDER


Hounsfield_units = {
  "air":[-1000],
  "lung":[-700,-500],
  "fat":[-100,-84,-70,-50],
  "water":[0],
  "csf":[8,15],
  "white_matter":[20,30],
  "gray_matter":[37,45],
  "blood":[30, 40, 45, 65, 70],
  "muscle":[40],
  "soft tissue":[100, 300],
  "hematoma":[40,90],
  "liver":[40,60],
  "spleen":[35,55],
  "kidney":[20,40],
  "soft_tissue, contrast":[100,300],
  "bone (cancellous is low end, cortical high end)":[700,1000,3000]}


def scale_to_unit_interval(ndar, eps=1e-8):
  """
  Scales all values in the ndarray ndar to be between 0 and 1 
  """
  ndar = ndar.copy()
  ndar -= ndar.min()
  ndar *= 1.0 / (ndar.max() + eps)
  return ndar

def tile_raster_images(X, img_shape, tile_shape, tile_spacing=(0, 0), scale_rows_to_unit_interval=True, output_pixel_vals=True):
  """
  Transform an array with one flattened image per row, into an array in
  which images are reshaped and layed out like tiles on a floor.
  Args:
    X: a 2-D array in which every row is a flattened image.
    Img_shape: tuple; (height, width)
    Tile_shape: tuple; (rows, cols)
    Output_pixel_vals: if output should be pixel values (i.e. int8 values) or floats
    Scale_rows_to_unit_interval: if the values need to be scaled before being plotted to [0,1] or not
  Returns:
    Array suitable for viewing as an image, a 2-d array with same dtype as X
  """

  assert len(img_shape) == 2
  assert len(tile_shape) == 2
  assert len(tile_spacing) == 2

  out_shape = [
    (ishp + tsp) * tshp - tsp
    for ishp, tshp, tsp in zip(img_shape, tile_shape, tile_spacing)
  ]

  if isinstance(X, tuple):
    assert len(X) == 4
    # Create an output numpy ndarray to store the image
    if output_pixel_vals:
      out_array = np.zeros((out_shape[0], out_shape[1], 4),
                  dtype='uint8')
    else:
      out_array = np.zeros((out_shape[0], out_shape[1], 4),
                  dtype=X.dtype)

    #colors default to 0, alpha defaults to 1 (opaque)
    if output_pixel_vals:
      channel_defaults = [0, 0, 0, 255]
    else:
      channel_defaults = [0., 0., 0., 1.]

    for i in range(4):
      if X[i] is None:
        # if channel is None, fill it with zeros of the correct
        # dtype
        dt = out_array.dtype
        if output_pixel_vals:
          dt = 'uint8'
        out_array[:, :, i] = np.zeros(
          out_shape,
          dtype=dt
        ) + channel_defaults[i]
      else:
        # use a recurrent call to compute the channel and store it
        # in the output
        out_array[:, :, i] = tile_raster_images(
          X[i], img_shape, tile_shape, tile_spacing,
          scale_rows_to_unit_interval, output_pixel_vals)
    return out_array

  else:
    # if we are dealing with only one channel
    H, W = img_shape
    Hs, Ws = tile_spacing

    # generate a matrix to store the output
    dt = X.dtype
    if output_pixel_vals:
      dt = 'uint8'
    out_array = np.zeros(out_shape, dtype=dt)

    for tile_row in range(tile_shape[0]):
      for tile_col in range(tile_shape[1]):
        if tile_row * tile_shape[1] + tile_col < X.shape[0]:
          this_x = X[tile_row * tile_shape[1] + tile_col]
          if scale_rows_to_unit_interval:
            # if we should scale values to be between 0 and 1
            # do this by calling the `scale_to_unit_interval`
            # function
            this_img = scale_to_unit_interval(
              this_x.reshape(img_shape))
          else:
            this_img = this_x.reshape(img_shape)
          # add the slice to the corresponding position in the
          # output array
          c = 1
          if output_pixel_vals:
            c = 255
          out_array[
            tile_row * (H + Hs): tile_row * (H + Hs) + H,
            tile_col * (W + Ws): tile_col * (W + Ws) + W
          ] = this_img * c
    return out_array
      

def image_displayer(npa=None, image_shape=(256,256), name="_", display_image=False, save_image=True, output_dir=""):
  """
  takes an image array of (depth,height,width,channel) and dispalys as a seires of tiles... might need to squeeze out batch size
  Args:
    npa
    display_image
    image_shape: tuple of (height,width)
  Returns:
  """
  npa = npa.reshape((npa.shape[0],-1))
  imgr = tile_raster_images(npa, image_shape, (5,8), tile_spacing=(0, 0), scale_rows_to_unit_interval=False, output_pixel_vals=False)

  fig = plt.figure()
  fig.set_size_inches(80, 48)
  #fig.savefig('/home/eko/projects/icahnc_cth/cth_cnn/results/image_no{0}.png'.format(image_no), dpi=100)
  if display_image:
    plt.imshow(imgr, interpolation='nearest', cmap=plt.cm.Greys_r, vmin=0, vmax=100)
  if save_image:
    plt.imsave(output_dir+'/image_no{0}.png'.format(name), imgr, cmap=plt.cm.Greys_r, vmin=0, vmax=100)
    plt.close("all")