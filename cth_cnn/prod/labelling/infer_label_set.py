import numpy as np
import pandas as pd
import sys, os
import re
from collections import Counter, defaultdict

#Custom imports
sys.path.insert(0,"/home/eko/lib/icd_helpers")
import icd9, bigtable_nlp, confusion_matrix
 
from tqdm import tqdm
#NLP imports
import nltk
import gensim
from gensim.models.word2vec import Word2Vec
import multiprocessing
#ML imports
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.ensemble import ExtraTreesClassifier
from sklearn.naive_bayes import BernoulliNB, MultinomialNB
from sklearn.pipeline import Pipeline
from sklearn.svm import SVC
from sklearn.linear_model import SGDClassifier
from sklearn import metrics
from sklearn.metrics import accuracy_score
from sklearn.cross_validation import cross_val_score, cross_val_predict
from sklearn.cross_validation import StratifiedShuffleSplit
from sklearn import preprocessing
from sklearn.preprocessing import LabelBinarizer, FunctionTransformer

import logging
logger = logging.getLogger()
logger.setLevel(logging.INFO)
formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
fh = logging.FileHandler('/home/eko/projects/icahnc_cth/cth_cnn/logs/3dcnn_dataset.log')
fh.setLevel(logging.INFO)
fh.setFormatter(formatter)
logger.addHandler(fh)
ch = logging.StreamHandler()
ch.setLevel(logging.INFO)
ch.setFormatter(formatter)
logger.addHandler(ch)


#define decoder object
class label_decoder(object):
  """
  Label_decoder class lets us move labels up or down the hiearchy defined by our label_groups
  """
  def __init__(self,df_label_grouping, coded_to_norm):
    self.label_grouping_cols = df_label_grouping.columns.tolist()
    self.norm_to_general = {}
    self.coded_to_norm=coded_to_norm
    for i in range(len(df_label_grouping)):
      self.norm_to_general[df_label_grouping.iloc[i]["LABELS"]]=[
        df_label_grouping.iloc[i][j] for j in self.label_grouping_cols]
  
  def description(self,meg_label):
    try:
      return self.coded_to_norm[meg_label]
    except KeyError as e:
      pass
  
  def generalize(self, meg_description, level):
    try:
      return self.norm_to_general[meg_description][level]
    except KeyError as e:
      pass


def get_training_set(y_col, thresh, vec, df_BigTable, label_cols, feature_cols):
  """
  Returns a training set of data. Will threshold labels based on Thresh from the avg. Reader
  values
  Args:
    y_col: target column
    thresh: int in (0,1) to threshold the average reader score on
    vec: embedding vectorizer
    df_BigTable: the BigTable
  Returns:
    X_train: npa of training features
    y_train: npa of training cols
  """

  #X_train and y_train
  Xy_train = df_BigTable[["doc"]+feature_cols+[label_cols[y_col]]].dropna()

  #Docs
  X_doc_train = Xy_train["doc"].as_matrix()
  X_embed_train = vec.transform(X_doc_train)
  
  #Metadata features
  try:
    X_feat_train = Xy_train[feature_cols]
    X_feat_train = pd.get_dummies(X_feat_train)[["age", "Ordered to Exam Completed (minutes)","is_stat_True", "gender_Male","location_Emergency","location_Inpatient","location_Outpatient"]].as_matrix()
    #Joined features
    X_train = np.concatenate((X_feat_train, X_embed_train), axis=1)
  except KeyError as e:
    print(e)
    print("PROCEEDING WITH EMBEDDINGS ONLY")
    X_train = X_embed_train

  #labels + threshold y_train on reviewer threshold... if anyone saw positive, we'll look for majority vote flag
  y_train = Xy_train[label_cols[y_col]].as_matrix()
  y_train[y_train >= thresh]=1
  y_train[y_train < thresh]=0
  
  return X_train, y_train


def get_inference_set(vec, df_BigTable, feature_cols):
  """
  Builds two numpy arrays ready for inference, a set of features + a set of the corresponding accession_id
  Args:
    vec: a w2vvectorizer
    df_BigTable: the BigTable dataframe that we'll carve our inference data out of
  """

  #X_train and X_accessions
  Xy_train = df_BigTable[["doc"]+feature_cols+["accession_id"]].dropna()
  
  #X_accessions
  X_accessions = Xy_train["accession_id"].as_matrix()
  
  #Docs
  X_doc_train = Xy_train["doc"].as_matrix()
  X_doc_train = np.array([list(map(lambda x:x.lower(),i)) for i in X_doc_train])
  X_embed_train = vec.transform(X_doc_train)
  
  #Metadata features
  X_feat_train = Xy_train[feature_cols]
  X_feat_train = pd.get_dummies(X_feat_train)[["age", "Ordered to Exam Completed (minutes)","is_stat_True", "gender_Male","location_Emergency","location_Inpatient","location_Outpatient"]].as_matrix()

  #Joined features
  X_inference = np.concatenate((X_feat_train, X_embed_train), axis=1)
  
  return X_inference, X_accessions


def infer_label_npa(model, X_train, y_train, X_infer):
  """
  Fit a model to the given training set and return an inferred set of labels
  Args:
    model: a sklearn model
    X_train: array, a numpy array of training samples
    y_train: array, a matching numpy array of training labels
    X_infer: array, an array of features for the entire dataset to infer labels on
  Returns:
    y_infer: array, a 1D array of labels for the entire dataset
  """
  #fit model
  model.fit(X_train,y_train)
  y_infer = np.array(model.predict(X_infer)) #model
  return y_infer


def get_column_groups(path_to_label_grouping, path_to_coded_dataset, path_to_normal_dataset):
  """
  Returns a list of the various groupings of columns in the BigTable
  Args:
    bunch of paths
  Returns:
    feature_cols: features to be used for subsequent modeling in encoded format
    feature_cols_norm: features to be used for subsequent modeling in English format
    meta_cols: metadata (accession_id)
    coded_cols: a list of all coded columns
  """
  df_label_grouping = pd.read_csv(path_to_label_grouping)
  
  df_coded = pd.read_excel(path_to_coded_dataset)
  df_normal = pd.read_excel(path_to_normal_dataset)
  meta_cols = ["accession_id"]
  coded_cols = df_coded.columns.tolist()
  normal_cols = df_normal.columns.tolist()
  feature_cols = [col for col in coded_cols if col not in meta_cols]

  coded_to_norm = {coded_cols[i]:normal_cols[i] for i in range(len(coded_cols))}

  megd = label_decoder(df_label_grouping, coded_to_norm)
  feature_cols_norm = [megd.description(col) for col in feature_cols]

  return feature_cols, feature_cols_norm, meta_cols, coded_cols


def generate_bigtable_cth(path_to_joe, path_to_javin, path_to_meg, path_to_montage,
  path_to_label_grouping, path_to_coded_dataset, path_to_normal_dataset):
  """
  Joins 
  """
  df_joe = pd.read_csv(path_to_joe)
  df_javin = pd.read_csv(path_to_javin)
  df_meg = pd.read_excel(path_to_meg)
  
  feature_cols, feature_cols_norm, meta_cols, coded_cols = get_column_groups(
    path_to_label_grouping, path_to_coded_dataset, path_to_normal_dataset)

  df_merged = df_joe.merge(df_javin,on='accession_id').merge(df_meg,on="accession_id")
  merged_cols = df_merged.columns.tolist()
  merged_accession_ids = df_merged["accession_id"]

  examples = []
  for i in tqdm(range(len(merged_accession_ids))):
    example={}
    example["accession_id"]=df_meg.iloc[i]["accession_id"]
    for col in feature_cols:
      example[megd.description(col)] = np.mean([df_joe.iloc[i][col],df_javin.iloc[i][col],df_meg.iloc[i][col]])
    examples.append(example)

  #Make into new df and get accession back to INt type
  df_merged = pd.DataFrame(examples)
  df_merged["accession_id"] = df_merged["accession_id"].astype(int)

  df_montage = bigtable_nlp.join_montage_files(path_to_montage)
  df_metadata = df_montage[["accession_id", "Patient Age", "Is Stat", "Patient Sex", 'Ordered to Exam Completed (minutes)']]
  df_montage = bigtable_nlp.df_to_corpus(df_data=df_montage, stem_words=False)
  
  df_BigTable = df_montage.merge(df_metadata, on="accession_id").merge(df_merged, how="outer", on="accession_id")
  temp_data_dir = "/home/eko/projects/icahnc_cth/cth_cnn/dev/data"
  df_BigTable.to_pickle(os.path.join(temp_data_dir,"CTH_bigtable.p"))


def infer_labels(temp_data_dir, thresh, model,
  path_to_label_grouping, path_to_coded_dataset, path_to_normal_dataset):
  """
  """
  logger.info("LOADING DATA INTO MEM")
  df_BigTable = pd.read_pickle(os.path.join(temp_data_dir,"CTH_bigtable.p"))
  #KEY! THESE ARE THE FEATURE COLS FROM  BIGTABLE THAT WE'LL USE FOR INFERENCE IN ADDITION TO WORD EMBEDDINGS

  #TODO(eko): can uncomment if we want to use metadata for inference (not good b/c later we'll want to use for label pred)
  #feature_cols = ["age","gender","is_stat","location","Ordered to Exam Completed (minutes)"]
  feature_cols = None

  _, label_cols, meta_cols, coded_cols = get_column_groups(
    path_to_label_grouping, path_to_coded_dataset, path_to_normal_dataset)

  X_corpus = df_BigTable["doc"].dropna().as_matrix()
  X_corpus = np.array([list(map(lambda x:x.lower(),i)) for i in X_corpus])

  #train joint
  logger.info("TRAINING W2V MODEL")
  w2v_model = Word2Vec(X_corpus, size=300, iter=20, window=5, min_count=3, workers=6)
  w2v_embeds = {w: vec for w, vec in zip(w2v_model.wv.index2word, w2v_model.wv.syn0)}
  logger.info(w2v_model.most_similar(positive=["normal"]))
  logger.info(w2v_model.most_similar(positive=["hemorrhage"]))

  logger.info("INITIALIZING VECTORIZER")
  vec = bigtable_nlp.TfidfEmbeddingVectorizer(w2v_embeds)
  vec = vec.fit(X_corpus,np.array([]))

  logger.info("STARTING INFERENCE ENGINE")
  #Get inference set
  X_infer, X_accessions = get_inference_set(vec, df_BigTable, feature_cols)
  #select vectorizer and  initialize with the word embeddings

  npa_dict = {}
  for col in tqdm(range(len(label_cols))):
    X_train, y_train = get_training_set(col, thresh, vec, df_BigTable, label_cols, feature_cols)  
    try:
      y_pred = infer_label_npa(model,X_train,y_train,X_infer)
    except ValueError as e:
      logger.info(e)
      y_pred=np.zeros(len(X_infer)) 
    npa_dict[label_cols[col]]=y_pred
    
  npa_dict["accession_id"]=X_accessions

  df_results = pd.DataFrame(npa_dict)

  df_results.to_pickle(os.path.join(temp_data_dir,"results.p"))
  df_results.to_csv(os.path.join(temp_data_dir,"results.csv"))


def infer_hand_engineered_labels(path_to_label_grouping, path_to_normal_dataset, path_to_results,
  path_to_coded_dataset, output_csv):
  """
  """
  df_label_grouping = pd.read_csv(path_to_label_grouping)
  df_normal = pd.read_excel(path_to_normal_dataset)
  df_results = pd.read_csv(path_to_results)
  df_coded = pd.read_excel(path_to_coded_dataset)

  meta_cols = ["accession_id"]
  coded_cols = df_coded.columns.tolist()
  normal_cols = df_normal.columns.tolist()
  label_cols = df_results.columns.tolist()
  coded_to_norm = {coded_cols[i]:normal_cols[i] for i in range(len(coded_cols))}
  megd = label_decoder(df_label_grouping, coded_to_norm)

  #TODO(eko): cleanup this shit
  accession_array = df_results["accession_id"].as_matrix()
  feature_cols = df_results.columns.tolist()
  feature_cols.remove("Unnamed: 0")
  feature_cols.remove("accession_id")
  feature_cols.remove("my_first_instrument_complete")
  feature_cols.remove("redcap_data_access_group")
  try:
    feature_cols.remove("has_critical_finding")
  except:
    pass

  #THIS MAKES HIGHER LEVEL LABEL DICTS
  two_gen_clin_groups = {k:[] for k in df_label_grouping["general_clinical_groups"].unique()}
  three_acuity_groups = {k:[] for k in df_label_grouping["acuity_groups"].unique()}
  four_crit_groups = {k:[] for k in df_label_grouping["CLINICAL_ENDPOINT: label_criticality_groups"].unique()}
  #gen clin groups
  for col in feature_cols:
    print(col)
    two_gen_clin_groups[megd.generalize(col,2)].append(df_results[col].as_matrix())
    three_acuity_groups[megd.generalize(col,3)].append(df_results[col].as_matrix())
    four_crit_groups[megd.generalize(col,4)].append(df_results[col].as_matrix())
  for k,v in two_gen_clin_groups.items():
    #two_gen_clin_groups[k] = np.clip(np.sum(v,axis=0),0,1)
    two_gen_clin_groups[k] = np.sum(v,axis=0)
  for k,v in three_acuity_groups.items():
    #three_acuity_groups[k] = np.clip(np.sum(v,axis=0),0,1)
    three_acuity_groups[k] = np.sum(v,axis=0)
  for k,v in four_crit_groups.items():
    #four_crit_groups[k] = np.clip(np.sum(v,axis=0),0,1)
    four_crit_groups[k] = np.sum(v,axis=0)

  #then make a new dataframe, and some of the hand engineered generalized features:
  df_new=pd.concat([pd.DataFrame(four_crit_groups),pd.DataFrame(three_acuity_groups),pd.DataFrame(two_gen_clin_groups)], axis=1)
  df_merged = pd.concat([df_new, df_results],axis=1)
  print("SHAPE BEFORE HAND ENGINEERING:{0}".format(df_merged.shape))
  df_merged["gen_hemorrhage"]=df_merged["hemorrhage"].ix[:,0]
  df_merged["gen_hemorrhage_at_2"] = np.array([i*1 for i in [df_merged["gen_hemorrhage"] > 2]]).T
  df_merged["gen_hemorrhage_at_1"] = np.array([i*1 for i in [df_merged["gen_hemorrhage"] > 1]]).T
  df_merged["crit_at_44"] = np.array([i*1 for i in [df_merged["critical"] > 44]]).T
  df_merged["crit_at_10"] = np.array([i*1 for i in [df_merged["critical"] > 10]]).T
  df_merged["crit_at_8"] = np.array([i*1 for i in [df_merged["critical"] > 8]]).T
  df_merged["crit_at_5"] = np.array([i*1 for i in [df_merged["critical"] > 5]]).T
  df_merged["crit_at_4"] = np.array([i*1 for i in [df_merged["critical"] > 4]]).T
  print("SHAPE AFTER HAND ENGINEERING:{0}".format(df_merged.shape))

  #Save to disc as results_extended
  df_merged.to_csv(output_csv)


if __name__ == "__main__":
  path_to_joe="/home/eko/data/ICAHNC1/meg_curated/NLPValidationDataSet_DATA_2017-04-21_1506_joe.csv"
  path_to_javin="/home/eko/data/ICAHNC1/meg_curated/NLPValidationDataSet_DATA_2017-04-21_1508_javin.csv"
  path_to_meg="/home/eko/data/ICAHNC1/meg_curated/NLPValidationDataSet_DATA_2017-04-21_1813_meg.xlsx"
  path_to_montage = "/home/eko/data/ICAHNC1/reports_raw"
  path_to_label_grouping="/home/eko/data/ICAHNC1/meg_curated/CTH_Label_groups_02142017.csv"
  path_to_normal_dataset="/home/eko/data/ICAHNC1/meg_curated/NLP_Validation_Data_Set_coded.xlsx"
  path_to_coded_dataset="/home/eko/data/ICAHNC1/meg_curated/NLPValidationDataSet_DATA_2017-04-21_1813_meg.xlsx"
  #CHOOSE A MODEL HERE... make sure it scales nicely (linear models)
  model = SVC(kernel="linear")
  #model=SGDClassifier(loss='log',penalty='elasticnet',alpha=0.001, l1_ratio=0.15)
  generate_new_bigtable = False

  #TODO(eko): the label decoder should be instantiate with all of these random files, and then instances
  #of the label_decoder should be passed into these methods rather than constantly re-making it.

  if generate_new_bigtable:
    generate_bigtable_cth(
      path_to_joe=path_to_joe,
      path_to_javin=path_to_javin,
      path_to_meg=path_to_meg,
      path_to_montage=path_to_montage,
      path_to_label_grouping=path_to_label_grouping,
      path_to_normal_dataset=path_to_normal_dataset,
      path_to_coded_dataset=path_to_coded_dataset)

  infer_labels(
    temp_data_dir="/home/eko/projects/icahnc_cth/cth_cnn/dev/data",
    thresh=0.5,
    model=model,
    path_to_label_grouping=path_to_label_grouping,
    path_to_normal_dataset=path_to_normal_dataset,
    path_to_coded_dataset=path_to_coded_dataset)

  infer_hand_engineered_labels(
    path_to_label_grouping=path_to_label_grouping,
    path_to_normal_dataset=path_to_normal_dataset,
    path_to_results="/home/eko/projects/icahnc_cth/cth_cnn/dev/data/results.csv",
    path_to_coded_dataset=path_to_coded_dataset,
    output_csv="/home/eko/projects/icahnc_cth/cth_cnn/dev/data/results_expanded_06072017.csv")