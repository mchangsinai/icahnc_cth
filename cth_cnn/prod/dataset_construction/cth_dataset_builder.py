"""
This takes the numpy arrays and BigTable and makes a final dataset json for the CNN to feed on
"""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import numpy as np
import pandas as pd
pd.set_option('display.max_colwidth', 1000) #b/c something perverse w/ col width and data retreival

from tqdm import tqdm
import os
import sys
import json

#from PIL import Image
#For logging stuff, need to be good about this, get logger here
import logging
logger = logging.getLogger()
logger.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
fh = logging.FileHandler('/home/eko/projects/icahnc_cth/cth_cnn/logs/3dcnn_dataset.log')
fh.setLevel(logging.DEBUG)
fh.setFormatter(formatter)
logger.addHandler(fh)
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
ch.setFormatter(formatter)
logger.addHandler(ch)


TARGET_CLASSES_DICT = {
  "nsurg" : ["normal", "aneurysm coil", "acute SDH", "SAH", ],
  "surgical" : ["normal", "gen_hemorrhage_at_1"], #hemorrhage_sum = 1 if EDH,ICH,IVH,SAH,SDH = 1
  "QC" : ["normal","degraded___1"],
  "critical_finding" : ["normal","has_critical_finding"],
  "crit_at_4" : ["normal", "crit_at_4"],
  "crit_at_8" : ["normal", "crit_at_8"],
  "crit_at_10" : ["normal", "crit_at_10"],
  "crit_at_44" : ["normal", "crit_at_44"],
  "stroke" : ["normal", "stroke/infarction/ischemia (infarction, venous thrombosis)"],
  "hemorrhage" : ["normal", "hemorrhage"],
  "big_3" : ["normal", "stroke/infarction/ischemia (infarction, venous thrombosis)", "hemorrhage"],
  "expert_choice" :["normal","ICH","acute SDH","SAH","IVH","SDH, age indeterminate (or subacute SDH)",
    "stroke/infarction/ischemia (infarction, venous thrombosis)","sinus disease (bony sinuses, not venous)",
    "non-obstructive/communicating hydrocephalus","atrophy (brain)"],
  "greater_than_95AUC": ["normal","blastic bone tumor","hyperostosis","aneurysm coil","bone tumor/hyperostosis","burr hole",
    "vasogenic edema","carotid siphon calcification","maxillary sinus disease","ethmoid sinus disease","sphenoid sinus disease",
    "artificial lens/lens replacement","degraded___1","right VPS","nasogastric tube","vertebral artery calcification",
    "encephalomalacia","SAH","post-op from infratentorial surgery","sinus disease (bony sinuses, not venous)","scan is post-operative",
    "foreign objects","right to left midline shift","left supratentorial encephalomalacia","extra-axial non-specific calcification",
    "chronic small vessel disease","vascular abnormality (aneurysm, arteriovenous malformation)","edema (CNS only)",
    "congenital abnormality","post-op from supratentorial surgery","intra-axial non-specific fluid collection","IVH"]
  }


def to_categorical(y, num_classes=None):
    """Converts a class vector (integers) to binary class matrix.
    E.g. for use with categorical_crossentropy.
    # Arguments
        y: class vector to be converted into a matrix
            (integers from 0 to num_classes).
        num_classes: total number of classes.
    # Returns
        A binary matrix representation of the input.
    """
    y = np.array(y, dtype='int').ravel()
    if not num_classes:
        num_classes = np.max(y) + 1
    n = y.shape[0]
    categorical = np.zeros((n, num_classes))
    categorical[np.arange(n), y] = 1
    return categorical


def assign_label(accession_id, df_labels, target_label, target_classes):
  label_idx = target_classes.index(target_label)
#   print("target label:",target_label)
#   print("target idx", label_idx)
  labels = df_labels.at[int(accession_id), target_label]
  labels = np.array([int(labels * label_idx)])
#   print("pre_cat labels:", labels)
#   print("num classes", len(target_classes))
  labels = to_categorical(labels, len(target_classes))
#   print("post cat labels", labels)
  #make numpy array a list so json will love it
  return labels.tolist()


def assign_multilabel(accession_id, df_labels, target_classes):
  labels = df_labels[target_classes].loc[int(accession_id)].as_matrix()
  labels = labels.astype(int)
  return labels.tolist()


def build_labelled_dataset(df_labels, image_dir, output_dir, target_classes,
  input_shape, train_test_split, label_type, balance, df_bigtable):
  """
  Method for assigning labels to the CTH volumes that are alreaddy stored in the data directory and labelled by accession_id.
  Pass it a masked dataframe and run on each part of the mask to get train/test split.
  Args:
    df_labels: a dataframe containing the labels as binary columns... note that a single scan can have multiple labels
    image_dir: the directory containing the volumes
    output_dir: the directory where we should output the json files
    target_classes: what classes we want to use from the dataframe for constructing our volume->target maps
    train_test_split: a str indicating whether this is the training or validation set
  Returns:
    None: writes a .json to disk containing mappings from every scan to a one-hot vector of the labels which are
      present for that scan.
  """
  #Get the accession no's and paths of the volumes. Accession ids in a list, and paths in a dict keyed by accession
  logger.warn("============== WRITING NEW {0} DATASET LABELS ==============".format(train_test_split))
  logger.warn("LABEL_TYPE:{0}".format(label_type))
  logger.warn("BALANCING MODE:{0}".format(balance))
  logger.info("number of scanned accessions in {0}: {1}".format(train_test_split,len(df_bigtable["sopinstanceuid"].unique())))
  #Get the accession no's from the labels
  logger.info("number of labelled accesions: {0}".format(len(df_labels.index)))
  #Find the intersection of the two sets of accession numbers... we'll use these
  bigtable_sopuids = df_bigtable[df_bigtable["patient_accession"].isin(df_labels.index.tolist())]["sopinstanceuid"].tolist()
  logger.info("intersection of labelled and scanned in {0}: {1}".format(train_test_split,len(bigtable_sopuids)))


  #make example dicts just like we would a proto... I use jsons like protos
  features_count = {i:0 for i in target_classes}
  features_dict = {}
  features_dict["examples"] = []
  features_dict["metadata"] = {}

  balancing_normals_added = 0 #counter for if we want to balance normal cases
  for i in range(0,len(bigtable_sopuids)):
    sopinstanceuid = bigtable_sopuids[i]
    accession_id = df_bigtable[df_bigtable["sopinstanceuid"]==sopinstanceuid]["patient_accession"].tolist()[0]
    data_path = df_bigtable[df_bigtable["sopinstanceuid"]==sopinstanceuid]["img_path"].to_string(index=False)
    label_array = assign_multilabel(accession_id=accession_id, df_labels=df_labels, target_classes=target_classes)
    patient_age = df_bigtable[df_bigtable["sopinstanceuid"]==sopinstanceuid]["patient_age"].to_string(index=False)
    patient_sex = df_bigtable[df_bigtable["sopinstanceuid"]==sopinstanceuid]["patient_sex"].to_string(index=False)
    patient_service = df_bigtable[df_bigtable["sopinstanceuid"]==sopinstanceuid]["patient_service"].to_string(index=False)
    patient_physician = df_bigtable[df_bigtable["sopinstanceuid"]==sopinstanceuid]["patient_physician"].to_string(index=False)

    if label_type=="binary_relevance":
      #check if any of the other labels are 1... so slice out normal which is the first element of list
      if not (np.sum([i for i in label_array[1:]]) > 0):
        #create one-hot list for normal and append it
        label_array = [0 for i in range(len(label_array))]
        label_array[0] = 1
        #increment normal
        features_count[target_classes[0]]+=1
        #checks if we want to balance, if so only append normal when fewer normal cases then other labels
        if balance: 
          if balancing_normals_added < sum([value for key, value in features_count.items() if key != target_classes[0]]):
            balancing_normals_added+=1
            features_dict["examples"].append({
              "sopinstanceuid" : sopinstanceuid,
              "filenames" : data_path,
              "labels" : label_array,
              "patient_age" : patient_age,
              "patient_sex" : patient_sex,
              "patient_service" : patient_service,
              "patient_physician" : patient_physician})
        elif not balance:
          #increment normal
          features_count[target_classes[0]]+=1
          features_dict["examples"].append({
            "sopinstanceuid" : sopinstanceuid,
            "filenames" : data_path,
            "labels" : label_array,
            "patient_age" : patient_age,
            "patient_sex" : patient_sex,
            "patient_service" : patient_service,
            "patient_physician" : patient_physician}) 
      else:
        #loop through target_classes but skip i=0 which is normal
        for i in range(1,len(label_array)):
          #check the i'th position in the array
          if label_array[i] == 1:
            label_array = [0 for i in range(len(label_array))]
            label_array[i] = 1
            features_count[target_classes[i]]+=1
            features_dict["examples"].append({
              "sopinstanceuid" : sopinstanceuid,
              "filenames" : data_path,
              "labels" : label_array,
              "patient_age" : patient_age,
              "patient_sex" : patient_sex,
              "patient_service" : patient_service,
              "patient_physician" : patient_physician})     
    elif label_type=="multilabel":
      #count up labels
      for i in range(0,len(label_array)):
        features_count[target_classes[i]]+=label_array[i]
      features_dict["examples"].append({
        "sopinstanceuid" : sopinstanceuid,
        "filenames" : data_path,
        "labels" : label_array,
        "patient_age" : patient_age,
        "patient_sex" : patient_sex,
        "patient_service" : patient_service,
        "patient_physician" : patient_physician}) 

  #save the metadata in the json for later initializing the graph
  logger.info("num_classes = {0} | input shape = {1}".format(len(target_classes),input_shape))
  features_dict["metadata"]["num_classes"] = len(target_classes)
  features_dict["metadata"]["input_shape"] = input_shape
  features_dict["metadata"]["num_examples"] = len(features_dict["examples"])
  features_dict["metadata"]["target_classes"] = target_classes
  logger.info("FEATURES_COUNT:{0}".format(features_count))
  #note that we have to recast keys as strings when re-load b/c JS doesn't let us have string keys and JSON is from JS
  features_dict["metadata"]["cost_weights"] = {target_classes.index(k):features_count[target_classes[0]]/v for k,v in features_count.items()}

  #OUTPUT SOME SUMMARY RESULTS
  logger.info("label summary in {0}: {1}".format(train_test_split,target_classes))
  logger.info("label_counts in {0}: {1}".format(train_test_split,np.sum([i["labels"] for i in features_dict["examples"]],axis=0)))
  logger.info("BALANCED_NORMALS IN DATASET:{0}".format(balancing_normals_added))
  #logger.info("ORIGINAL UNBALANCED COUNTS: {0}".format(features_count))
  logger.info("FINAL SIZE OF {0}: {1}".format(train_test_split, len(features_dict["examples"])))

  # save json to file:
  features_filename = os.path.join(output_dir,str(train_test_split)+"_cth_labels_features.json")
  with open(features_filename, 'w') as f:
    json.dump(features_dict, f)
  logger.info("FINISHED GENERATING CTH JSON DATA")             


if __name__ == "__main__":

  #=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=
  ############################################ CONFIG ######################################
  #=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=

  #Path to the silver standard labels from the NLP algorithm output
  SILVER_STANDARD_LABELS = "/home/eko/projects/icahnc_cth/cth_cnn/dev/data/results_expanded_06072017.csv"

  #Path to the imaging directory
  # IMAGE_DIR = "/home/eko/projects/icahnc_cth/cth_cnn/data/train_full"
  # CANONICAL_SHAPE = (40, 512, 512, 1)
  IMAGE_DIR = "/home/eko/projects/icahnc_cth/cth_cnn/data/train_normed/vols"
  IMAGE_METADATA = "/home/eko/projects/icahnc_cth/cth_cnn/data/train_normed/BigTable.csv"
  CANONICAL_SHAPE = (128, 128, 128, 1) #TODO(eko): should add in dim when write originally... not hack it in later in the gen. FIX!

  #Path to the image-label mapping json
  OUTPUT_DIR = "/home/eko/projects/icahnc_cth/cth_cnn/data/labels"

  #the labelset to use for writing the json
  DATASET_NAME = "crit_at_10"
  #DATASET_NAME = "hemorrhage"

  BALANCE = True
  #The type of labelling to do. Either multilabel or binary_relevance
  LABEL_TYPE = "binary_relevance" #"binary_relevance" or "multilabel"

  #whether to write a third TEST set of data utilizing only gold standard labels
  WRITE_TEST=False

  #MAKE NEW BIGTABLE
  MAKE_NEW_BT = False

  #=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=
  ######################################## BUILD THE DATA ##################################
  #=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=

  #make new BT from metadata folder
  if MAKE_NEW_BT:
    metadata_dir = "/home/eko/projects/icahnc_cth/cth_cnn/data/train_normed/metadata"
    metadata_list = [os.path.join(metadata_dir,i) for i in os.listdir(metadata_dir)]
    json_list = [json.load(open(i,"r")) for i in metadata_list]
    df_bigtable = pd.DataFrame(json_list)
    df_bigtable.to_csv(IMAGE_METADATA)
  else:
    #load metadata
    df_bigtable = pd.read_csv(IMAGE_METADATA)

  #load up the labels from the silver standard labels and split into train and test sets
  df_labels = pd.read_csv(SILVER_STANDARD_LABELS)
  df_labels = df_labels.set_index(["accession_id"])
  df_labels.sample(frac=1) 
  msk = np.random.rand(len(df_labels)) < 0.90
  df_train = df_labels[msk]
  df_val = df_labels[~msk]


  #write the train json
  build_labelled_dataset(
    df_labels=df_train,
    image_dir=IMAGE_DIR,
    output_dir=OUTPUT_DIR,
    target_classes=TARGET_CLASSES_DICT[DATASET_NAME],
    input_shape=CANONICAL_SHAPE,
    train_test_split="train",
    label_type=LABEL_TYPE,
    balance=BALANCE,
    df_bigtable=df_bigtable)

  #write the validation json
  build_labelled_dataset(
    df_labels=df_val,
    image_dir=IMAGE_DIR,
    output_dir=OUTPUT_DIR,
    target_classes=TARGET_CLASSES_DICT[DATASET_NAME],
    input_shape=CANONICAL_SHAPE,
    train_test_split="val",
    label_type=LABEL_TYPE,
    balance=False,
    df_bigtable=df_bigtable)

  if WRITE_TEST:
    #GET FROM BIGTABLE AND WRITE A TEST DATASET
    df_meg = pd.read_excel("/home/eko/data/ICAHNC1/meg_curated/NLP_Validation_Data_Set_coded.xlsx")
    meta_cols = ["accession_id",'redcap_data_access_group','my_first_instrument_complete']
    coded_cols=df_meg.columns.tolist()
    feature_cols = [col for col in coded_cols if col not in meta_cols]
    df_BigTable=pd.read_pickle(os.path.join(temp_data_dir,"CTH_bigtable.p"))
    df_BigTable_test=df_BigTable[["accession_id"]+feature_cols].dropna()

    build_labelled_dataset(
      df_labels=df_BigTable_test,
      image_dir=IMAGE_DIR,
      output_dir=OUTPUT_DIR,
      target_classes=TARGET_CLASSES_DICT[DATASET_NAME],
      input_shape=CANONICAL_SHAPE,
      train_test_split="test",
      label_type=LABEL_TYPE,
      balance=False)